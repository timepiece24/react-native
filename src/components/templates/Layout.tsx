import React, { FC } from 'react';
import { View, Dimensions, Platform, StatusBar } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Loader from '../elements/Loader';

import allColors from 'src/constants/allColors';

const { width } = Dimensions.get('screen');

import { useTheme } from 'src/constants/theme/ThemeProvider';
import { appStyles } from 'src/constants';

export interface Props {
  didFinishInitialAnimation?: boolean;
  children?: any;
  color?: string;
  noStatus?: boolean;
}

const Layout: FC<Props> = (props) => {
  const { colors, isDark } = useTheme();
  return (
    <>
      <StatusBar barStyle={'light-content'} backgroundColor={allColors.Transparent} translucent={true} />

      <View
        style={{
          flex: 1,

          backgroundColor: props.color ? props.color : colors.background,
        }}>
        <View
          style={{
            width: width,

            ...appStyles.CenterContent,
            marginTop: Platform.OS === 'ios' ? wp(8) : 0,
          }}
        />
        {!props.didFinishInitialAnimation ? (
          <Loader didFinishInitialAnimation={props.didFinishInitialAnimation} />
        ) : (
            <View
              //   {...props}
              style={{
                flex: 1,
                width: width,
                backgroundColor: 'transparent',
                zIndex: 0,
                marginBottom: Platform.OS === 'ios' ? wp(12) : wp(0),
              }}>
              {props.children}
            </View>
          )}
      </View>
    </>
  );
};

export default Layout;
