import React, { Children, FC } from 'react';
import {
  I18nManager,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  IconAntMaterialCommunityIcons,
  useNavigation,
  wp,
  IconAntIonicons,
  hp,
} from 'src/all';
import { allColors, typography, Images } from 'src/constants';
import { CommonActions } from '@react-navigation/native';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { ScreensNames } from 'src/ScreensNames';
export interface Props {
  title?: string;
  children?: any;
}
const Header: FC<Props> = (props) => {
  const navigation = useNavigation();
  const { colors, isDark } = useTheme();

  function renderTitle() {
    if (typeof props.title === 'string') {
      return (
        <View style={styles.title}>
          <Text style={styles.title}>{props.title}</Text>
        </View>
      );
    }

    if (!props.title) {
      return <Text style={{ width: '40%' }} />;
    }

    return props.title;
  }

  function renderLeft() {
    if (!props.title) {
      return <Image source={Images.logo} style={styles.logoStyle} />;
    } else {
      return (
        <TouchableOpacity
          style={styles.button}
          onPress={(): void => {
            navigation.navigate(ScreensNames.NAVIGATION_HOME_SCREEN)
            // navigation.dispatch(CommonActions.goBack());
          }}>
          <IconAntMaterialCommunityIcons
            name={I18nManager.isRTL ? 'chevron-right' : 'chevron-left'}
            color={allColors.WHITE}
            style={{
              fontWeight: 'bold',
              fontSize: 30,
            }}
          />
        </TouchableOpacity>
      );
    }
  }

  function renderRight() {
    return (
      <TouchableOpacity
        style={styles.button}
        onPress={(): void => {
          // navigation.goBack();
        }}>
        <IconAntIonicons
          name={'notifications'}
          color={allColors.WHITE}
          style={{ fontWeight: 'bold', fontSize: 25, padding: -10 }}
        />
      </TouchableOpacity>
    );
  }
  return (
    <View style={{ backgroundColor: colors.header, paddingTop: wp(10) }}>
      <View style={[styles.navBar, { backgroundColor: colors.header }]}>
        {renderLeft()}
        {renderTitle()}
        {renderRight()}
      </View>
      {props.children}
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  button: {
    padding: 5,
    position: 'relative',
    width: '10%',

  },
  title: {
    width: '85%',
    ...typography.TextHeader,
    textAlign: 'left',
    fontSize: 16,
    lineHeight: 25,
    color: allColors.WHITE,
  },
  navBar: {
    width: 'auto',
    height: hp(2.5) * 4.125,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    paddingBottom: wp(3),
    paddingTop: wp(2),
    zIndex: 5,
    opacity: 1,
  },
  logoStyle: {
    width: '38%',
    height: '100%',
    tintColor: allColors.WHITE,

    position: 'relative',
  },
});
