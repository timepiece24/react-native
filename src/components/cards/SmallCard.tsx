import React, { FC, ReactElement, useState, useEffect, useRef } from 'react';

import { useSelector, useDispatch } from 'react-redux';

import {
  TouchableWithoutFeedback,
  Dimensions,
  StyleSheet,
  Image,
} from 'react-native';
import {
  View,
  Text,
  // TextInput,
} from 'react-native-animatable';

import { hp, wp, Localize, IconMaterialIcons, useNavigation } from '../../all';

import { typography, Images, allColors } from '../../constants';
import { Block, Button } from 'src/components';

import { useTheme } from 'src/constants/theme/ThemeProvider';
import { border, radius } from 'src/constants/dimensions';
import { ScreensNames } from 'src/ScreensNames';

const { width, height } = Dimensions.get('screen');

const cardWidth = wp(90);

interface Props {
  item?: any;
}

const SmallCard: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();
  const navigation = useNavigation()

  const dispatch = useDispatch();

  useEffect(() => { }, []);
  const onclick = () => navigation.navigate(ScreensNames.NAVIGATION_PRODUCT_DETAILS_SCREEN);
  return (
    <Block
      center
      key={props.item.id}
      style={[styles.container, { backgroundColor: colors.cardBackgroung }]}>
      <View
        animation={'zoomIn'}
        delay={100}
        duration={600}
        style={{
          justifyContent: 'center',
          paddingTop: wp(1),
        }}>

        <Image source={{ uri: props.item.image }} style={styles.imageStyle} />

        <Block
          style={{
            // marginTop: wp(25),
            marginStart: wp(0.5),
          }}>

          <Button
            title={props.item.title}
            // title={Localize.t('product.details')}
            textStyle={{
              ...typography.headerWelcom,
              color: allColors.WHITE,
              fontSize: 10,
              marginVertical: -3,

            }}
            style={{
              backgroundColor: allColors.GOLD,
              paddingHorizontal: wp(2),
              paddingVertical: wp(2),
              maxWidth: wp(23),
              marginVertical: -3,

            }}
         
            onPress={onclick}
          />
        </Block>
      </View>
    </Block>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: radius.noRadius,
    borderWidth: border.noborder,
    // elevation: 2,
    paddingVertical: wp(3),
    paddingHorizontal: wp(3),
    marginBottom: wp(1),
    marginHorizontal: wp(1),
    marginVertical: wp(1),
    backgroundColor: allColors.LIGHTBLUE,
    width: wp(31),
  },
  ListaElementStyle: {
    ...typography.TextRegular13,
    color: allColors.SPACEGRAY,
    marginVertical: 5,
    lineHeight: wp('7'),
  },
  imageStyle: {
    height: wp(30),
    width: wp(30),
    // width: 70,
    // height: 70,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  defaultText: {
    ...typography.TextRegular12,
    textAlign: 'center',
  },
});

export default SmallCard;
