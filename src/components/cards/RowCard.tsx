import React, { FC, ReactElement, useState, useEffect, useRef } from 'react';

import { useSelector, useDispatch } from 'react-redux';

import {
  TouchableWithoutFeedback,
  Dimensions,
  StyleSheet,
  Image,
} from 'react-native';
import {
  View,
  Text,
  // TextInput,
} from 'react-native-animatable';

import { hp, wp, Localize, IconMaterialIcons , useNavigation} from '../../all';

import { typography, Images, allColors } from '../../constants';
import { Block, Button } from 'src/components';

import { useTheme } from 'src/constants/theme/ThemeProvider';
import { border, radius } from 'src/constants/dimensions';
import { ScreensNames } from 'src/ScreensNames';

const { width, height } = Dimensions.get('screen');

const cardWidth = wp(90);

interface Props {
  item?: any;
}

const RowCard: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();
  const navigation = useNavigation()

  const dispatch = useDispatch();

  useEffect(() => { }, []);
  
  const onclick = () => navigation.navigate(ScreensNames.NAVIGATION_PRODUCT_DETAILS_SCREEN);
  return (
    <Block
      center
      key={props.item.id}
      style={[styles.container, { backgroundColor: colors.cardBackgroung }]}>
      <View
        animation={'zoomIn'}
        delay={100}
        duration={600}
        style={{
          // alignItems: 'center',
          justifyContent: 'center',
          //flexDirection: 'row',
          paddingTop: wp(1),
        }}>
        <Block>
          <IconMaterialIcons
            name={'star'}
            style={{
              color: allColors.GOLD,
              fontSize: 25,
              position: 'absolute',
              right: wp(0),
            }}
          />
        </Block>

        <Block row>
          <Image source={{ uri: props.item.image }} style={styles.imageStyle} />

          <Block
            style={{
              // marginTop: wp(25),
              marginStart: wp(5),
            }}>
            <Text
              style={{
                color: colors.cardText,
                ...typography.headerWelcom,
                fontSize: 16,
              }}>
              {props.item.title}
            </Text>

            <Button
              title={Localize.t('product.details')}
              textStyle={{
                color: colors.cardText,
                ...typography.headerWelcom,
                fontSize: 14,
                marginTop: wp(-1),

              }}
              style={{
              }}
              onPress={onclick}
            />
            <Text
              style={{
                ...typography.headerWelcom,
                fontSize: 20,
                color: allColors.GOLD,
                textAlign: 'left',
                marginTop: wp(5),

              }}>
              {props.item.price}
            </Text>
          </Block>
         
        </Block>
       
      </View>
    </Block>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: radius.noRadius,
    borderWidth: border.noborder,
    // elevation: 2,
    paddingVertical: wp(3),
    paddingHorizontal: wp(3),
    marginBottom: wp(1),
    marginHorizontal: wp(1),
    marginVertical: wp(1),
    backgroundColor: allColors.LIGHTBLUE,
    width: wp(90),
  },
  ListaElementStyle: {
    ...typography.TextRegular13,
    color: allColors.SPACEGRAY,
    marginVertical: 5,
    lineHeight: wp('7'),
  },
  imageStyle: {
    height: wp(25),
    width: wp(25),
    // width: 70,
    // height: 70,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  defaultText: {
    ...typography.TextRegular12,
    textAlign: 'center',
  },
});

export default RowCard;
