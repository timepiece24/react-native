import React, { FC } from 'react';
import {
  TouchableWithoutFeedback,
  StyleSheet,
  Platform,
  TouchableNativeFeedback,
} from 'react-native';

interface Props {
  children?: any;

  onPress?: () => void;
}

const Touchable: FC<Props> = (props) => {
  return Platform.OS === 'ios' ? (
    <TouchableWithoutFeedback onPress={props.onPress}>
      {props.children}
    </TouchableWithoutFeedback>
  ) : (
    <TouchableNativeFeedback onPress={props.onPress}>
      {props.children}
    </TouchableNativeFeedback>
  );
};

export default Touchable;
