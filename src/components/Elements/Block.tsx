import React, {
  FC,
  ReactElement,
  useRef,
  useState,
  useEffect,
  Component,
} from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import { Image, Text, View } from 'react-native-animatable';
import { animation } from 'src/constants';

export interface Props {
  style?: object | boolean;
  row?: boolean;
  flex?: boolean;
  center?: boolean;
  middle?: boolean;
  top?: boolean;
  bottom?: boolean;
  right?: boolean;
  left?: boolean;
  space?: boolean;

  height?: boolean;
  shadowColor?: boolean;
  width?: boolean | number;
  duration?: number;
  delay?: number;

  safe?: boolean;
  children?: any;
  password?: boolean;
  motion?: string;
  noMotion?: boolean;

  value?: string;
  onChangeText?: (text: string) => void;
}
const Block: FC<Props> = (props): ReactElement => {
  const {
    row,
    flex,
    center,
    middle,
    top,
    bottom,
    right,
    left,
    space,
    height,
    motion,
    shadowColor,
    width,
    safe,
    children,
    style,
    delay,
    duration,
    noMotion,
    ...otherProps
  } = props;
  const styleBlock = [
    style,
    styles.block,
    row && styles.row,
    flex && { flex: flex === true ? 1 : flex },
    center && styles.center,
    middle && styles.middle,
    top && styles.top,
    bottom && styles.bottom,
    right && styles.right,
    left && styles.left,
    space && { justifyContent: `space-${space}` },

    height && { height },
    width && { width },
    shadowColor && { shadowColor },
  ];

  if (safe) {
    return (
      <SafeAreaView style={styleBlock} {...props}>
        {children}
      </SafeAreaView>
    );
  }
  const animated = motion ? motion : noMotion ? null : animation.zoomIn; //animation.zoomIn;
  const delayValue = delay ? delay : 600;
  const durationValue = duration ? duration : 400;

  return (
    <View
      animation={animated}
      delay={delayValue}
      duration={durationValue}
      style={styleBlock}
      {...props}>
      {children}
    </View>
  );
};

export default Block;

const styles = StyleSheet.create({
  block: {
    flexDirection: 'column',
  },
  row: {
    flexDirection: 'row',
  },
  middle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  center: {
    alignItems: 'center',
    alignSelf: 'center',
  },
  left: {
    alignItems: 'flex-start',
  },
  right: {
    alignItems: 'flex-end',
  },
  top: {
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
  },
  bottom: {
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
  },
});
