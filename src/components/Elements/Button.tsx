import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import { IconAntMaterialCommunityIcons } from 'src/all';
import Touchable from './Touchable';
import { Image, Text, View } from 'react-native-animatable';
import Block from './Block';
interface Props {
  children?: any;
  title?: string;
  style?: object | undefined;
  textStyle?: object;
  withIcon?: boolean;
  icon?: string;
  iconStyle?: object;
  iconLeft?: boolean;
  iconRight?: boolean;
  onPress?: () => void;
}

const StrokedButton: FC<Props> = (props) => {
  function renderIcon() {
    return (
      <IconAntMaterialCommunityIcons
        name={props.icon}
        style={props.iconStyle}
      />
    );
  }
  return (
    <Touchable onPress={props.onPress}>
      <Block style={props.style} {...props}>
        {props.iconLeft && props.withIcon && renderIcon()}
        <Text style={props.textStyle}>{props.title}</Text>
        {props.iconRight && props.withIcon && renderIcon()}
      </Block>
    </Touchable>
  );
};
export default StrokedButton;

const styles = StyleSheet.create({
  button: {},
});
