import { Toast } from 'native-base';
import { typography, allColors } from 'src/constants';
import { wp } from 'src/all';

export default function showToast(message: string) {
  return Toast.show({
    text: message,
    //   buttonText: 'حسنًا',
    //   buttonTextStyle: { ...typography.TextBlack },
    //   buttonStyle: {},
    duration: 2000,
    type: 'success',
    position: 'bottom',
    textStyle: {
      ...typography.TextRegular11,
      color: allColors.WHITE,
      textAlign: 'center',
    },
    style: {
      marginBottom: wp(30),
      alignSelf: 'center',
      backgroundColor: allColors.CustomBlack,
      opacity: 0.9,
      maxWidth: wp(70),
      minWidth: wp(70),
      borderRadius: 15,
    },
  });
}
