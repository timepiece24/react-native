import React, { FC } from 'react';
import { View, ActivityIndicator } from 'react-native';
import allColors from 'src/constants/allColors';

const Loader: FC<{ didFinishInitialAnimation: boolean, color?: string }> = ({
  didFinishInitialAnimation = false,
  color
}) => {
  return (
    <View style={{ flex: 1, backgroundColor: 'transparent' }}>
      <ActivityIndicator
        style={{
          flex: 1,
          opacity: !didFinishInitialAnimation ? 1.0 : 0.0,
        }}
        animating={true}
        size="large"
        color={color ? color : allColors.CustomBlack}
      />
      {/* <Spinner /> */}
    </View>
  );
};

export default Loader;
