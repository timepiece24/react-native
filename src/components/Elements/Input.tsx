import React, {
  FC,
  ReactElement,
  useRef,
  useState,
  useEffect,
  Component,
} from 'react';
import { Text, StyleSheet, TextInput } from 'react-native';
import { View } from 'react-native-animatable';
import { allColors, typography } from 'src/constants';
import { IconAntIonicons, wp, hp, Localize } from 'src/all';

import { Input } from 'react-native-elements';

export interface Props {
  placeholder?: string;
  keyboardType?: string;
  returnKeyType?: string;
  textInputStyle?: object;
  name?: string;
  isEnabled?: boolean;
  icon?: string;
  isIcon?: boolean;
  password?: boolean;
  iconColor?: string;
  help?: string;
  bottomHelp?: boolean;
  style?: object;
  value?: string;
  onChangeText?: (text: string) => void;
}

const MyInput: FC<Props> = (props): ReactElement => {
  const [isFocused, setisFocused] = useState(false);
  const [noStar, setnoStar] = useState(false);
  const inputEl = useRef(null);

  const {
    icon,
    isIcon,
    iconColor,
    help,
    bottomHelp,
    style,
    ...otherProps
  } = props;

  const helpContent = help && (
    <View>
      <Text style={styles.helpText}>{help}</Text>
      <Text style={styles.helpTextstar}>{' *'}</Text>
    </View>
  );

  useEffect(() => {
    inputEl.current.focus();
  });
  // focus = (): void => this.textInputRef.focus()
  return (
    <View
      style={
        style
          ? {
              marginTop: 10,
              marginBottom: 2,
            }
          : styles.container
      }>
      <View
        // style={[styles.textInputWrapper, {borderColor}]}
        style={style ? style : styles.textInputWrapper}>
        <View style={{ flexDirection: 'row' }}>
          {isIcon && (
            <IconAntIonicons
              name={icon}
              style={{
                fontSize: 20,
                bottom: -10,
                width: 20,
                marginStart: 10,
                marginEnd: 10,
                color: iconColor,
              }}
            />
          )}
          <Input
            // containerStyle={styles.fullInput}
            disabledInputStyle={{ backgroundColor: '#ddd' }}
            inputContainerStyle={{}}
            errorMessage="Oops! that's not correct."
            errorStyle={{
              color: allColors.MUTED,
              // textAlign: ' center',
            }}
            errorProps={{}}
            inputStyle={styles.textInputStyle}
            // label=""
            // labelStyle={{
            //     position: 'absolute',
            //     top: -25,
            //     backgroundColor: allColors.DARKBLUE,
            // }}
            // labelProps={{}}
            // autoCapitalize={false}
            // autoCorrect={false}
            // leftIcon={<Icon name="account-outline" size={20} />}
            // leftIconContainerStyle={{}}
            // rightIcon={<Icon name="close" size={20} />}
            // rightIconContainerStyle={{}}
            placeholder={Localize.t('placeholder.name')}
          />
        </View>
      </View>
      {/* {bottomHelp && !noStar && helpContent} */}
    </View>
  );
};
export default MyInput;
const styles = StyleSheet.create({
  container: {
    marginTop: 2,
    marginBottom: 10,

    borderRadius: 15,
    borderColor: allColors.MUTED,
    borderWidth: 1,
    height: 40,
    backgroundColor: '#FFFFFF',
    width: wp(90),
    elevation: 1,
    marginHorizontal: 10,
    // fontFamily: Theme.FONTS.PRIMARY,
    color: allColors.BLACK,
  },
  textInputWrapper: {
    height: 42,
    marginBottom: 2,
    borderBottomWidth: 0,
    ...typography.TextInput,
    borderBottomColor: allColors.DARKBLUE,
  },
  textInput: {
    flex: 1,
    ...typography.TextInput,

    color: allColors.DARKBLUE,
    // margin: IS_ANDROID ? -1 : 0,
    height: 42,
    padding: 0,
  },
  helpText: {
    ...typography.TextBlack,
    fontSize: 8,
    marginTop: 0,
    textShadowOffset: { width: 1, height: 1 },
  },
  helpTextstar: {
    marginStart: 0,
    bottom: 4,
    ...typography.TextInputHelp,
    fontSize: 15,

    color: allColors.RED,
  },
});
