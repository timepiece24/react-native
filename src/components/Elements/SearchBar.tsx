import React, { FC, ReactElement, useState, useEffect } from 'react';
import { View } from 'react-native';
import { IconAntDesign, Localize } from 'src/all';
import { appStyles } from 'src/constants';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Input } from 'react-native-elements';

const SearchBar: FC<{}> = (): ReactElement => {
    const { colors, isDark } = useTheme();

    useEffect(() => { }, []);

    return (
        <View
            style={{ ...appStyles.searchBarContainer }}>
            <Input
                // containerStyle={styles.fullInput}
                disabledInputStyle={{ backgroundColor: '#ddd' }}
                inputContainerStyle={{
                    ...appStyles.searchBarStyle,
                    backgroundColor: colors.cardBackgroung,
                }}
                inputStyle={{ ...appStyles.textSearchBarStyle, }}
                placeholder={Localize.t('placeholder.search')}
                // leftIcon={<IconAntDesign name={'search1'}/>}
                // leftIconContainerStyle={{  }}
                rightIcon={
                    <IconAntDesign
                        name={'search1'}
                        style={{ fontSize: 20, marginEnd: 10 }}
                    />
                }
            />
        </View>
    );
};

export default SearchBar;
