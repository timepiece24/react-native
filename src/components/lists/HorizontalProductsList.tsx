import React, { FC, ReactElement, useState, useEffect } from 'react';
import { FlatList } from 'react-native';

import { wp } from 'src/all';

import SmallCard from '../cards/SmallCard';


interface Props {
  data?: any;
}

const HorizontalProductsList: FC<Props> = (props): ReactElement => {
  const renderItem = (item: any) => {
    return <SmallCard item={item.item} {...props} />;
  };
  return (
    <FlatList
      // key={'v'} //'h'
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{
        marginVertical: wp(5),
        // paddingEnd: wp(10),
      }}
      // style={{...appStyles.listStyle,}}
      horizontal={true}
      keyExtractor={(item) => item.id}
      data={props.data}
      renderItem={renderItem}
      onEndReachedThreshold={0.02}
    />
  );
};

export default HorizontalProductsList;
