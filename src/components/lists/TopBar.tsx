import React, { FC, ReactElement, useState, useEffect } from 'react';

import { TouchableOpacity, Text, ScrollView } from 'react-native';
import { View } from 'react-native-animatable';
import { Localize, wp } from 'src/all';
import { allColors, typography } from 'src/constants';
import { border } from 'src/constants/dimensions';
import { useTheme } from 'src/constants/theme/ThemeProvider';

interface Props {
  items?: any;
  selectedIndex?: any;
  onChange?: any;
  style?: any;
  underline?: any;
}

const TopBar: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();

  const { items,
    selectedIndex,
    onChange,
    style,
    underline } = props;

  return (
    <View
      style={[styles.container, underline && styles.underline, style && style]}
    >
      {items &&
        items.map((item: string, index: number) => {
          let isActive = false;
          if (selectedIndex !== undefined && selectedIndex === index)
            isActive = true;

          let activeStyle: object = styles.itemActive;
          if (underline) activeStyle = styles.itemActiveUnderline;

          let activeTextStyle: object = styles.textActive;
          if (underline) activeTextStyle = styles.textActiveUnderline;

          return (
            <TouchableOpacity
              onPress={() => onChange(index)}
              key={item}
              style={[
                styles.item,
                { backgroundColor: colors.cardBackgroung },
                props.underline && styles.itemUnderline,
                isActive && activeStyle,
              ]}
            >
              <Text
                style={[
                  styles.text,
                  { color: colors.search },
                  props.underline && styles.textUnderline,
                  isActive && activeTextStyle,
                ]}
              >
                {Localize.t(`profileTexts.${item}`)}
              </Text>
              {underline && isActive && (
                <View
                  style={{
                    height: 5,
                    borderBottomColor: allColors.BLUE,
                    borderBottomWidth: 3,
                    position: 'absolute',
                  }}
                />
              )}
            </TouchableOpacity>
          );
        })}
    </View>
  );
}
export default TopBar;

const styles = {
  container: {
    // backgroundColor: allColors.LIGHTBLUE, // '#3F51B5',
    flex: 1,
    flexDirection: 'row',

    borderRadius: 1,
    // paddingStart: 3,
    // paddingEnd: 3,
    marginHorizontal: wp(5),
    paddingVertical: 3,
  },
  underline: {
    borderWidth: 0,
  },
  item: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',

    paddingVertical: 10,
    marginHorizontal: wp(1),
    marginVertical: 4,
    minWidth: 40,
    borderWidth: border.noborder,
    paddingHorizontal: wp(1),
  },
  itemUnderline: {
    borderBottomWidth: 0,
    borderBottomColor: allColors.WHITE, // '#e3e3e3',
  },
  itemActive: {
    // backgroundColor:  allColors.WHITE,
    backgroundColor: allColors.GOLD, // '#3F51B5',
    elevation: 15,
    opacity: 0.9,
  },
  itemActiveUnderline: {
    borderBottomWidth: 4,
    borderBottomColor: allColors.LIGHTBLUE,
  },
  text: {
    ...typography.TextLight10,
    opacity: 1,
    fontSize: 12,
  },
  textUnderline: {
    // color: '#a6a6a6',
    color: allColors.WHITE,
  },
  textActive: {
    opacity: 1,
    color: allColors.LIGHTBLUE,
    marginHorizontal: 5,
  },
  textActiveUnderline: {
    color: allColors.WHITE,
  },
};

