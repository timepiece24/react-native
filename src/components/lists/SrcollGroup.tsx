import React, { FC, ReactElement, useState, useEffect } from 'react';

import { TouchableOpacity, Text, ScrollView } from 'react-native';
import { View } from 'react-native-animatable';
import { allColors, typography } from 'src/constants';
import { border } from 'src/constants/dimensions';
import { useTheme } from 'src/constants/theme/ThemeProvider';

interface Props {
  items?: any;
  selectedIndex?: any;
  onChange?: any;
  style?: any;
  underline?: any;
}

const SrcollGroup: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();

  return (
    <ScrollView
      horizontal
      showsHorizontalScrollIndicator={false}
      style={[
        styles.container,
        props.underline && styles.underline,
        props.style && props.style,
      ]}>
      {props.items &&
        props.items.map((item: any, index: any) => {
          let isActive = false;
          if (
            props.selectedIndex !== undefined &&
            props.selectedIndex === index
          )
            isActive = true;

          let activeStyle: any = styles.itemActive;
          if (props.underline) activeStyle = styles.itemActiveUnderline;

          let activeTextStyle: any = styles.textActive;
          if (props.underline) activeTextStyle = styles.textActiveUnderline;

          return (
            <TouchableOpacity
              onPress={() => props.onChange(index)}
              key={item.id || item}
              style={[
                styles.item,
                { backgroundColor: colors.cardBackgroung },
                props.underline && styles.itemUnderline,
                isActive && activeStyle,
              ]}>
              <Text
                style={[
                  styles.text,
                  { color: colors.cardText },
                  props.underline && styles.textUnderline,
                  isActive && activeTextStyle,
                ]}>
                {item.value || item}
              </Text>
              {props.underline && isActive && (
                <View
                  style={{
                    height: 5,
                    borderBottomColor: allColors.BLUE,
                    borderBottomWidth: 3,
                    position: 'absolute',
                  }}
                />
              )}
            </TouchableOpacity>
          );
        })}
    </ScrollView>
  );
};
export default SrcollGroup;

const styles = {
  container: {
    // backgroundColor: allColors.LIGHTBLUE, // '#3F51B5',
    flex: 1,
    flexDirection: 'row',

    borderRadius: 1,
    paddingStart: 3,
    paddingEnd: 3,
    paddingVertical: 3,
  },
  underline: {
    borderWidth: 0,
  },
  item: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',

    paddingVertical: 10,
    marginHorizontal: 1,
    marginVertical: 4,
    minWidth: 40,
    borderWidth: border.noborder,
    paddingHorizontal: 15,
  },
  itemUnderline: {
    borderBottomWidth: 0,
    borderBottomColor: allColors.WHITE, // '#e3e3e3',
  },
  itemActive: {
    // backgroundColor:  allColors.WHITE,
    backgroundColor: allColors.GOLD, // '#3F51B5',
    elevation: 15,
    opacity: 0.9,
  },
  itemActiveUnderline: {
    borderBottomWidth: 4,
    borderBottomColor: allColors.LIGHTBLUE,
  },
  text: {
    ...typography.TextLight10,
    opacity: 1,
    fontSize: 12,
  },
  textUnderline: {
    // color: '#a6a6a6',
    color: allColors.WHITE,
  },
  textActive: {
    opacity: 1,
    color: allColors.LIGHTBLUE,
    marginHorizontal: 5,
  },
  textActiveUnderline: {
    color: allColors.WHITE,
  },
};
