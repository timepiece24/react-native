import React, { FC, ReactElement, useState, useEffect } from 'react';
import {  FlatList } from 'react-native';

import { hp, wp } from 'src/all';

import { allColors, appStyles, Images } from 'src/constants';

import NormalCard from '../cards/NormalCard';


interface Props {
  data?: any;
  header?: any;
  footer?: any;
}

const ProductsList: FC<Props> = (props): ReactElement => {
  const renderItem = (item: any) => {
    //  console.log(item.item);
    return <NormalCard item={item.item} {...props} />;
  };
  return (

    <FlatList
      ListHeaderComponent={props.header}
      ListFooterComponent={props.footer}


      key={'h'} //'v'
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{
        marginBottom: wp(5),
      }}
      style={{
        ...appStyles.listStyle,
      }}
      horizontal={false}
      numColumns={2}
      keyExtractor={(item) => item.id}
      data={props.data}
      renderItem={renderItem}
      onEndReachedThreshold={0.02}
    />
  );
};

export default ProductsList;
