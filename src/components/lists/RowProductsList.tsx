import React, { FC, ReactElement, useState, useEffect } from 'react';
import { Dimensions, FlatList } from 'react-native';

import { hp, wp } from 'src/all';

import { allColors, appStyles, Images } from 'src/constants';

import RowCard from '../cards/RowCard';

const { width, height } = Dimensions.get('screen');

interface Props {
  data?: any;
  header?: any;
  footer?: any;
}

const RowProductsList: FC<Props> = (props): ReactElement => {
  const renderItem = (item: any) => {
    //  console.log(item.item);
    return <RowCard item={item.item} {...props} />;
  };
  return (
    <FlatList
      
      ListHeaderComponent={props.header}
      ListFooterComponent={props.footer}
      key={'h'} //'v'
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{
        marginBottom: wp(5),
      }}
      style={{
        // ...appStyles.listStyle,
      }}
      horizontal={false}
      numColumns={1}
      keyExtractor={(item) => item.id}
      data={props.data}
      renderItem={renderItem}
      onEndReachedThreshold={0.02}
    />
  );
};

export default RowProductsList;
