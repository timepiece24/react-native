import React, { useEffect, useState } from 'react';

import 'react-native-gesture-handler';
import { StatusBar, LogBox, I18nManager } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from 'state/store';
import RootNavigator from './navigation/SwitchNavigator';

import * as RNLocalize from 'react-native-localize';
import en from './translations/en.json';
import ar from './translations/ar.json';
import { AppearanceProvider, useColorScheme } from 'react-native-appearance';
import { ThemeProvider } from 'src/constants/theme/ThemeProvider';

import { Root } from 'native-base';
import { AsyncStorage } from 'src/all';
import { enableScreens } from 'react-native-screens';

import { StorageNames } from 'src/StorageNames';
import { removeFromStorage, saveToStorage } from './utils/StorageMethods';
import SplashScreen from 'src/screens/SplashScreen/SplashScreen';
const translationGetter = { en, ar };

enableScreens();

function App(): React.ReactElement {
  useEffect(() => {
    const { languageTag, isRTL } = RNLocalize.findBestAvailableLanguage(
      Object.keys(translationGetter),
    ) || { languageTag: 'en' };

    AsyncStorage.getItem(StorageNames.lang).then((value) => {
      removeFromStorage(StorageNames.lang);
      if (value) {
        if (value === languageTag) {
          saveToStorage(StorageNames.lang, languageTag);
        } else {
          saveToStorage(StorageNames.lang, value);
        }
      } else {
        if (languageTag === 'en') {
          I18nManager.forceRTL(false);
        } else if (languageTag === 'ar') {
          I18nManager.forceRTL(true);
        }
        saveToStorage(StorageNames.lang, languageTag);
      }
    });
  }, []);

  return <RootNavigator />;
}
function ProviderWrapper(): React.ReactElement {
  const [isDisplaySplashScreen, setIsDisplaySplashScreen] = useState(true);

  LogBox.ignoreAllLogs(true);
  useEffect(() => {
    setTimeout(() => {
      setIsDisplaySplashScreen(false);
    }, 4500);
  }, []);

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <AppearanceProvider>
          <ThemeProvider>
            <Root>{isDisplaySplashScreen ? <SplashScreen /> : <App />}</Root>
          </ThemeProvider>
        </AppearanceProvider>
      </PersistGate>
    </Provider>
  );
}

export default ProviderWrapper;
