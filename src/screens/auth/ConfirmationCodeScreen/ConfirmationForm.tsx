import React, { FC, ReactElement, useState, useMemo, useEffect } from 'react';
import { Image, Text, View } from 'react-native-animatable';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Form } from 'native-base';
import styles from '../AuthStyles';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Input } from 'react-native-elements';
import { loginSchema } from 'src/constants/validation';
import { Localize, useNavigation, wp, useTheme } from 'src/all';
import { allColors, animation } from 'src/constants';
import { Block, Button } from 'src/components';
import { ScreensNames } from 'src/ScreensNames';

type FormData = {
  name: string;
  email: string;
  password: string;
};

const LoginForm: FC<{}> = (props) => {
  const { colors } = useTheme();

  const navigation = useNavigation();

  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(loginSchema),
  });

  function renderQuestion() {
    return (
      <Block row center>
        <Text
          animation={animation.slideInDown}
          delay={600}
          duration={400}
          style={styles.questionTextStyle}>
          {Localize.t('validation.Resend code in')}
        </Text>

        {/* there is a timer in here, but not yet. */}
        <Button
          title={'00:58 Sec'}
          textStyle={styles.questionButtonStyle}
          {...props}
        />
      </Block>
    );
  }

  return (
    <KeyboardAwareScrollView contentContainerStyle={styles.container}>
      <Form>
        <Button
          title={Localize.t('buttons.submit')}
          style={styles.authButtonStyle}
          textStyle={styles.authButtonTextStyle}
          onPress={(): void => {
            navigation.navigate('BottomTabNavigator');
          }}
          {...props}
        />
        <Controller
          render={(props) => (
            <Block>
              <Input
                containerStyle={styles.fullInput}
                disabledInputStyle={{ backgroundColor: '#ddd' }}
                inputContainerStyle={{ borderColor: allColors.Transparent }}
                errorMessage=""
                errorStyle={styles.errorInputStyle}
                errorProps={{}}
                inputStyle={styles.textInputStyle}
                placeholder={Localize.t('screens.confirmationCode')}
              />
            </Block>
          )}
          control={control}
          name="confirmation"
          defaultValue=""
        />

        {renderQuestion()}
      </Form>
    </KeyboardAwareScrollView>
  );
};
export default LoginForm;
