import React, { FC, ReactElement, useState, useEffect } from 'react';
import {
  BackHandler,
  InteractionManager,
  TouchableOpacity,
  StatusBar,
  TextInput,
} from 'react-native';
import { Image, Text, View } from 'react-native-animatable';

import styles from '../AuthStyles';
import {
  wp,
  hp,
  Layout,
  AsyncStorage,
  Localize,
  useNavigation,
  LinearGradient,
  IconAntMaterialCommunityIcons,
} from 'src/all';
import { Input, Block } from 'src/components';
// import Localize from 'src/translations';
import { useTheme } from '@react-navigation/native';
import { DefaultNavigationProps } from 'src/constants/types';
import { Images, allColors } from 'src/constants';
import { ScreensNames } from 'src/ScreensNames';
import ConfirmationForm from './ConfirmationForm';
interface Props {
  navigation: DefaultNavigationProps<'default'>;
}

const ConfirmationCodeScreen: FC<Props> = (props): ReactElement => {
  const navigation = useNavigation();

  const { colors } = useTheme();

  const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState(
    false,
  );
  const [username, setusername] = useState('');
  const handleBackButtonClick = () => {
    navigation.navigate(ScreensNames.NAVIGATION_LOGIN_SCREEN);
    return true;
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButtonClick,
    );

    InteractionManager.runAfterInteractions(() => {
      setTimeout(function () {
        setDidFinishInitialAnimation(true);
      }, 1);
    });
    return () => backHandler.remove();
  }, []);
  return (
    <Layout
      didFinishInitialAnimation={didFinishInitialAnimation}
      color={allColors.DARKBLUE}>
      <StatusBar
        animated
        barStyle={'light-content'}
        backgroundColor={allColors.DARKBLUE}
      />

      <LinearGradient
        colors={['#122B47', '#122B47', '#0D1F3C', '#0D1F3C']}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={{ flex: 1 }}
        // locations={[0, 0.5, 0.6]}
        useAngle={true}
        angleCenter={{ x: 1, y: 1 }}
        angle={40}>
        <Block style={{ marginTop: wp(60) }}>
          <Block center>
            <Image
              source={Images.logo}
              style={{ width: wp(55), height: hp(11) }}
            />
          </Block>
          <ConfirmationForm {...props} />
        </Block>
      </LinearGradient>
    </Layout>
  );
};

export default ConfirmationCodeScreen;
