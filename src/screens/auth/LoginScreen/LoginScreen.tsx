import React, { FC, ReactElement, useState, useEffect } from 'react';
import {
  Text,
  View,
  Button,
  BackHandler,
  InteractionManager,
  TouchableOpacity,
  StatusBar,
  TextInput,
} from 'react-native';
import styles from '../AuthStyles';
import { Image } from 'react-native-animatable';
import { wp, hp, Layout, useNavigation, LinearGradient } from 'src/all';
import { Input, Block } from 'src/components';
// import Localize from 'src/translations';
import { useTheme } from '@react-navigation/native';
import { DefaultNavigationProps } from 'src/constants/types';
import { Images, allColors } from 'src/constants';
import LoginForm from './LoginForm';

interface Props {
  navigation: DefaultNavigationProps<'default'>;
}

const LoginScreen: FC<Props> = (props): ReactElement => {
  const navigation = useNavigation();

  const { colors } = useTheme();

  const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState(
    false,
  );
  const [username, setusername] = useState('');
  const handleBackButtonClick = () => {
    BackHandler.exitApp();
    return true;
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButtonClick,
    );

    InteractionManager.runAfterInteractions(() => {
      setTimeout(function () {
        setDidFinishInitialAnimation(true);
      }, 1);
    });
    return () => backHandler.remove();
  }, []);
  return (
    <Layout
      color={allColors.DARKBLUE}
      didFinishInitialAnimation={didFinishInitialAnimation}>
      <StatusBar
        animated
        barStyle={'light-content'}
        backgroundColor={allColors.DARKBLUE}
      />

      <LinearGradient
        colors={['#122B47', '#122B47', '#0D1F3C', '#0D1F3C']}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={{ flex: 1 }}
        // locations={[0, 0.5, 0.6]}
        useAngle={true}
        angleCenter={{ x: 1, y: 1 }}
        angle={40}>
        <Block center style={{ marginTop: wp(60) }}>
          {/* <IconLogo width={wp(80)} height={hp(10)} /> */}
          <Block center>
            <Image source={Images.logo} style={styles.logo} />
          </Block>
          <LoginForm {...props} />
        </Block>
      </LinearGradient>
    </Layout>
  );
};

export default LoginScreen;
