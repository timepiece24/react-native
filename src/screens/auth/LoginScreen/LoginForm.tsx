import React, { FC, ReactElement, useState, useMemo, useEffect } from 'react';
import { Image, Text, View } from 'react-native-animatable';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Form } from 'native-base';
import styles from '../AuthStyles';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Input } from 'react-native-elements';
import { loginSchema } from 'src/constants/validation';
import { Localize, useNavigation, wp, useTheme } from 'src/all';
import { allColors, animation } from 'src/constants';
import { Block, Button } from 'src/components';
import { ScreensNames } from 'src/ScreensNames';

type FormData = {
  name: string;
  email: string;
  password: string;
};

const LoginForm: FC<{}> = (props) => {
  const { colors } = useTheme();

  const navigation = useNavigation();

  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(loginSchema),
  });

  function renderQuestion() {
    return (
      <Block row center>
        <Text
          animation={animation.slideInDown}
          delay={600}
          duration={400}
          style={styles.questionTextStyle}>
          {Localize.t('validation.dontHaveAccount')}
        </Text>
        <Button
          title={Localize.t('screens.register')}
          textStyle={styles.questionButtonStyle}
          onPress={(): void => {
            navigation.navigate(ScreensNames.NAVIGATION_REGISTER_SCREEN);
          }}
          {...props}
        />
      </Block>
    );
  }

  return (
    <KeyboardAwareScrollView contentContainerStyle={styles.container}>
      <Form>
        <Controller
          render={(props) => (
            <Block>
              <Input
                containerStyle={styles.fullInput}
                disabledInputStyle={{ backgroundColor: '#ddd' }}
                inputContainerStyle={{ borderColor: allColors.Transparent }}
                errorMessage=""
                errorStyle={styles.errorInputStyle}
                errorProps={{}}
                inputStyle={styles.textInputStyle}
                placeholder={Localize.t('placeholder.name')}
              />
            </Block>
          )}
          control={control}
          name="username"
          defaultValue=""
        />
        <Controller
          render={(props) => (
            <Block>
              <Input
                containerStyle={styles.fullInput}
                disabledInputStyle={{ backgroundColor: '#ddd' }}
                inputContainerStyle={{ borderColor: allColors.Transparent }}
                errorMessage="Oops! that's not correct."
                errorStyle={styles.errorInputStyle}
                errorProps={{}}
                inputStyle={styles.textInputStyle}
                placeholder={Localize.t('placeholder.password')}
              />
            </Block>
          )}
          control={control}
          name="password"
          defaultValue=""
        />
        <Button
          title={Localize.t('screens.login')}
          style={styles.authButtonStyle}
          textStyle={styles.authButtonTextStyle}
          onPress={(): void => {
            navigation.navigate('BottomTabNavigator');
          }}
          {...props}
        />
        {renderQuestion()}
      </Form>
    </KeyboardAwareScrollView>
  );
};
export default LoginForm;
