import React, { FC, ReactElement, useState, useMemo, useEffect } from 'react';
import { Image, Text, View } from 'react-native-animatable';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Form, Radio } from 'native-base';
import styles from '../AuthStyles';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Input } from 'react-native-elements';
import { loginSchema, registerSchema } from 'src/constants/validation';
import { Localize, useNavigation, wp, useTheme } from 'src/all';
import { allColors, animation } from 'src/constants';
import { Block, Button } from 'src/components';
import { ScreensNames } from 'src/ScreensNames';

type FormData = {
  name: string;
  email: string;
  password: string;
};

const LoginForm: FC<{}> = (props) => {
  const { colors } = useTheme();
  const [checkBuyer, setCheckBuyer] = useState(true);
  const [checkDealer, setCheckDealer] = useState(false);

  const navigation = useNavigation();

  const { control, handleSubmit, errors } = useForm({
    resolver: yupResolver(registerSchema),
  });

  function renderQuestion() {
    return (
      <Block row center>
        <Text
          animation={animation.slideInDown}
          delay={600}
          duration={400}
          style={styles.questionTextStyle}>
          {Localize.t('validation.haveAccount')}
        </Text>
        <Button
          title={Localize.t('screens.login')}
          textStyle={styles.questionButtonStyle}
          onPress={(): void => {
            navigation.navigate(ScreensNames.NAVIGATION_LOGIN_SCREEN);
          }}
          {...props}
        />
      </Block>
    );
  }

  function renderChooseUser(type: string) {
    return (
      <Block middle row width={wp(50)}>
        <Radio
          color={allColors.WHITE}
          selectedColor={allColors.GOLD}
          selected={type == 'buyer' ? checkBuyer : checkDealer}
          style={{
            marginStart: 5,
          }}
          onPress={() => {
            if (type == 'buyer') {
              setCheckDealer(false);
              setCheckBuyer(true);
            } else {
              setCheckBuyer(false);

              setCheckDealer(true);
            }
          }}
        />
        <Text style={styles.userRadioChoose}>{Localize.t(`user.${type}`)}</Text>
      </Block>
    );
  }
  function renderDealerForm() {
    return (
      <View>
        {renderPhoneNumber()}
        {renderAddress()}
      </View>
    );
  }
  function renderPhoneNumber() {
    return (
      <Controller
        render={(props) => (
          <Block motion={animation.slideInDown} delay={0}>
            <Input
              containerStyle={styles.fullInput}
              disabledInputStyle={{ backgroundColor: '#ddd' }}
              inputContainerStyle={{ borderColor: allColors.Transparent }}
              keyboardType={'phone-pad'}
              errorMessage=""
              errorStyle={styles.errorInputStyle}
              errorProps={{}}
              inputStyle={styles.textInputStyle}
              placeholder={Localize.t('placeholder.phoneNumber')}
            />
          </Block>
        )}
        control={control}
        name="phoneNumber"
        defaultValue=""
      />
    );
  }
  function renderAddress() {
    return (
      <Controller
        render={(props) => (
          <Block motion={animation.slideInDown} delay={0}>
            <Input
              containerStyle={styles.fullInput}
              disabledInputStyle={{ backgroundColor: '#ddd' }}
              inputContainerStyle={{ borderColor: allColors.Transparent }}
              keyboardType={'default'}
              errorMessage=""
              errorStyle={styles.errorInputStyle}
              errorProps={{}}
              inputStyle={styles.textInputStyle}
              placeholder={Localize.t('placeholder.address')}
            />
          </Block>
        )}
        control={control}
        name="address"
        defaultValue=""
      />
    );
  }

  return (
    <KeyboardAwareScrollView contentContainerStyle={styles.container}>
      <Form>
        <Controller
          render={(props) => (
            <Block>
              <Input
                containerStyle={styles.fullInput}
                disabledInputStyle={{ backgroundColor: '#ddd' }}
                inputContainerStyle={{ borderColor: allColors.Transparent }}
                keyboardType={'email-address'}
                errorMessage=""
                errorStyle={styles.errorInputStyle}
                errorProps={{}}
                inputStyle={styles.textInputStyle}
                placeholder={Localize.t('placeholder.email')}
              />
            </Block>
          )}
          control={control}
          name="email"
          defaultValue=""
        />
        <Controller
          render={(props) => (
            <Block>
              <Input
                containerStyle={styles.fullInput}
                disabledInputStyle={{ backgroundColor: '#ddd' }}
                inputContainerStyle={{ borderColor: allColors.Transparent }}
                keyboardType={'default'}
                errorMessage=""
                errorStyle={styles.errorInputStyle}
                errorProps={{}}
                inputStyle={styles.textInputStyle}
                placeholder={Localize.t('placeholder.name')}
              />
            </Block>
          )}
          control={control}
          name="username"
          defaultValue=""
        />
        <Controller
          render={(props) => (
            <Block>
              <Input
                containerStyle={styles.fullInput}
                disabledInputStyle={{ backgroundColor: '#ddd' }}
                inputContainerStyle={{ borderColor: allColors.Transparent }}
                // keyboardType={'visible-password'}
                secureTextEntry={true}
                errorMessage="Oops! that's not correct."
                errorStyle={styles.errorInputStyle}
                errorProps={{}}
                inputStyle={styles.textInputStyle}
                placeholder={Localize.t('placeholder.password')}
              />
            </Block>
          )}
          control={control}
          name="password"
          defaultValue=""
        />
        <Controller
          render={(props) => (
            <Block>
              <Input
                containerStyle={styles.fullInput}
                disabledInputStyle={{ backgroundColor: '#ddd' }}
                inputContainerStyle={{ borderColor: allColors.Transparent }}
                // keyboardType={'default'}
                secureTextEntry={true}
                errorMessage=""
                errorStyle={styles.errorInputStyle}
                errorProps={{}}
                inputStyle={styles.textInputStyle}
                placeholder={Localize.t('placeholder.confirmPassword')}
              />
            </Block>
          )}
          control={control}
          name="confirmPassword"
          defaultValue=""
        />

        <Block flex row middle width={wp(90)}>
          {renderChooseUser('buyer')}
          {renderChooseUser('dealer')}
        </Block>

        {checkDealer && renderDealerForm()}

        <Button
          title={Localize.t('screens.register')}
          style={styles.authButtonStyle}
          textStyle={styles.authButtonTextStyle}
          onPress={(): void => {
            navigation.navigate(
              ScreensNames.NAVIGATION_CONFIRMATION_CODE_SCREEN,
            );
          }}
          {...props}
        />
        {renderQuestion()}
      </Form>
    </KeyboardAwareScrollView>
  );
};
export default LoginForm;
