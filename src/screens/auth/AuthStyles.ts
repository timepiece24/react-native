import { StyleSheet, Dimensions, Platform } from 'react-native';
import { color } from 'react-native-reanimated';

// import {Theme, allColors, AppStyle, typography} from 'src/constants';
import { hp, wp } from 'src/all';
import { allColors, typography, appStyles } from 'src/constants';
import { border } from 'src/constants/dimensions';
const { width, height } = Dimensions.get('screen');

const thumbMeasure = (width - 48 - 32) / 3;
const cardWidth = width - wp(2);
const cardHeight = height - wp(2);

export default StyleSheet.create({
  container: {
    paddingHorizontal: wp(5),
    paddingVertical: wp(3),
    // backgroundColor: 'red',
  },
  textInputStyle: {
    ...appStyles.textInputStyle,
  },
  fullInput: {
    ...appStyles.inputStyle,
  },

  authButtonStyle: {
    ...appStyles.buttonStyle,
  },
  authButtonTextStyle: {
    ...appStyles.textButttonStyle,
  },
  questionTextStyle: {
    ...typography.authQuestion,
    color: allColors.WHITE,
  },
  questionButtonStyle: {
    ...typography.authQuestion,
    color: allColors.GOLD,
    marginStart: 5,
    borderWidth: border.noborder,
    borderBottomWidth: border.thin,
    borderColor: allColors.GOLD,
    bottom: 3,
  },
  errorInputStyle: {
    ...appStyles.errorInputStyle,
  },
  userRadioChoose: {
    ...typography.authQuestion,
    color: allColors.WHITE,
    marginStart: 5,
  },
  logo: {
    ...appStyles.logo,
  },
});
