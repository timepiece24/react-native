import HomeScreen from './HomeScreen/HomeScreen';

import LoginScreen from './auth/LoginScreen/LoginScreen';
import RegisterScreen from './auth/RegisterScreen/RegisterScreen';
import ConfirmationCodeScreen from './auth/ConfirmationCodeScreen/ConfirmationCodeScreen';

import ProfileScreen from './profileScreens/ProfileScreen/ProfileScreen';
import SettingsScreen from './profileScreens/SettingsScreen/SettingsScreen';
import FavouritesScreen from './FavouritesScreen/FavouritesScreen';
import ProductsScreen from './ProductsScreen/ProductsScreen';
import ProductDetailsScreen from './ProductDetailsScreen/ProductDetailsScreen';
import SplashScreen from './SplashScreen/SplashScreen';

export const ScreensObject = {
  HomeScreen,
  SplashScreen,
  FavouritesScreen,
  ProfileScreen,
  SettingsScreen,
  ProductsScreen,
  ProductDetailsScreen,
  LoginScreen,
  RegisterScreen,
  ConfirmationCodeScreen,
};
