import React, { FC, ReactElement, useState, useEffect } from 'react';
import { Text, View } from 'react-native-animatable';
import styles from '../HomeScreenStyles';
import { Localize } from 'src/all';
import { useTheme } from 'src/constants/theme/ThemeProvider';

import { SearchBar } from 'src/components';

const HomeHeaderChildren: FC<{}> = (): ReactElement => {
  const { colors, isDark } = useTheme();

  useEffect(() => { }, []);

  const userName = Localize.t('home.UserName');
  return (
    <View style={{}}>
      <Text style={styles.headerName}>{userName}</Text>
      <Text style={styles.headerWelcom}>{Localize.t('home.welcometo')}</Text>

      <SearchBar />

    </View>
  );
};

export default HomeHeaderChildren;
