import React, { FC, ReactElement, useState, useEffect } from 'react';
import {
  View,
  Image,
  Text,
  ImageBackground,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { AsyncStorage, IconAntDesign, wp, Localize } from 'src/all';
import { allColors, appStyles, typography } from 'src/constants';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import HomeTitles from './HomeTitles';
import { Block, Button } from 'src/components';
import { color } from 'react-native-reanimated';
import { boxHight } from 'src/constants/dimensions';

// const { width, height } = Dimensions.get('screen');
const width = wp(95)
interface Props {
  data?: any;
}

const FeaturedItems: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();

  const [activeSlide, setActiveSlide] = useState(1);

  useEffect(() => {}, []);
  function pagination() {
    return (
      <Pagination
        dotsLength={props.data.length}
        dotColor={allColors.GOLD}
        activeDotIndex={activeSlide}
        containerStyle={{
          paddingVertical: 12,
        }}
        dotStyle={{
          marginHorizontal: -4,
        }}
        inactiveDotColor={allColors.PRIMARY}
        inactiveDotOpacity={1}
        inactiveDotScale={0.6}
        // carouselRef={_carousel}
        // tappableDots={!!_carousel}
      />
    );
  }
  function renderCarousel() {
    return (
      <Carousel
        firstItem={activeSlide}
        // ref={c => {
        //     _carousel = c;
        // }}
        data={props.data}
        layout={'stack'} //tinder , stack
        renderItem={(item: any) => {
          return (
            <ImageBackground
              source={{ uri: item.item.image }}
              style={styles.ItemSliderImage}
              resizeMode="cover"
              blurRadius={0.2}>
              <Block
                style={{
                  marginTop: wp(25),
                  marginStart: wp(10),
                }}>
                <Text
                  style={{
                    ...typography.TextBlack,
                    fontSize: 22,
                    color: allColors.WHITE,
                    marginVertical: -3,
                  }}>
                  {item.item.title}
                </Text>
                <Text
                  style={{
                    ...typography.headerWelcom,
                    fontSize: 14,
                    color: allColors.WHITE,
                    marginVertical: -3,
                  }}>
                  {item.item.title}
                </Text>

                <Button
                  title={Localize.t('product.viewNow')}
                  textStyle={{
                    ...typography.headerWelcom,
                    color: allColors.WHITE,
                    fontSize: 16,
                  }}
                  style={{
                    backgroundColor: allColors.GOLD,
                    paddingHorizontal: wp(5),
                    paddingVertical: wp(1),
                    width: wp(30),
                  }}
                  onPress={() => {}}
                />
                {/* {renderMedia(item.item.image)} */}
              </Block>
            </ImageBackground>
          );
        }}
        sliderWidth={width}
        itemWidth={width}
        onSnapToItem={(index): void => setActiveSlide(index)}
      />
    );
  }
  return (
    <View
      style={{ marginTop: boxHight.normal / 1.5 }}>
      <HomeTitles title={Localize.t('home.featured')} />
      {renderCarousel()}
      {props.data != undefined && pagination()}
    </View>
  );
};

export default FeaturedItems;
const styles = StyleSheet.create({
  ItemSliderImage: {
    height: width / 1.7,
    width: width,
    borderRadius: 15,
    alignSelf: 'center',
  },

  dot: {
    backgroundColor: 'transparent',
    //borderColor: colors.Black,
    borderWidth: 1,
    width: 10,
    height: 10,
    borderRadius: 10,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 0,
    marginBottom: '4%',
  },
  dotActive: {
    //backgroundColor: colors.yellow,
    width: 10,
    height: 10,
    borderRadius: 10,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 0,
    marginBottom: '4%',
  },
});
