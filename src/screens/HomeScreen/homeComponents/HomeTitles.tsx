import React, { FC, ReactElement, useState, useEffect } from 'react';
import { I18nManager, Text } from 'react-native';
import { allColors, appStyles, typography } from 'src/constants';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Block } from 'src/components';
import { Localize, wp } from 'src/all';
import { border } from 'src/constants/dimensions';

interface Props {
  title?: any;
  noView?: boolean;
}

const HomeTitles: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();

  useEffect(() => {}, []);

  return (
    <Block noMotion style={{ ...appStyles.homeTitlesBlock }}>
      <Text
        style={{
          ...typography.headerName,
          color: colors.search,
        }}>
        {props.title}
      </Text>
      {!props.noView && <Text
        style={{
          ...typography.viewAll,
          fontSize: 12,
          lineHeight: 20,

          borderWidth: border.noborder,
          borderBottomWidth: border.thick,
          borderColor: allColors.GOLD,
          position: 'absolute',
          right: wp(5),
          bottom: 1,
        }}>
        {Localize.t('home.viewAll')}
      </Text>}
    </Block>
  );
};

export default HomeTitles;
