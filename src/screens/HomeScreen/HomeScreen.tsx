import React, { FC, ReactElement, useState, useEffect } from 'react';
import { BackHandler, InteractionManager, ScrollView, StatusBar } from 'react-native';
import styles from './HomeScreenStyles';
import { Layout, Localize, wp } from 'src/all';
import { Block, Header } from 'src/components';
import HomeHeaderChildren from './homeComponents/HomeHeaderChildren';
import FeaturedItems from './homeComponents/FeaturedItems';
import SrcollGroup from 'src/components/lists/SrcollGroup';

import { boxHight } from 'src/constants/dimensions';
import { featuredItems, products } from 'src/constants/Entry';
import { useTheme } from 'src/constants/theme/ThemeProvider';

import ProductsList from 'src/components/lists/ProductsList';

import HomeTitles from './homeComponents/HomeTitles';
import { View } from 'react-native-animatable';

const HomeScreen: FC<{}> = (): ReactElement => {
  const { colors } = useTheme();
  const [index1, setIndex1] = useState();
  const [index2, setIndex2] = useState();

  const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState(
    false,
  );
  const handleBackButtonClick = () => {
    // console.log('Here Home')
    BackHandler.exitApp();
    return true;
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButtonClick,
    );

    InteractionManager.runAfterInteractions(() => {
      setTimeout(function () {
        setDidFinishInitialAnimation(true);
      }, 1);
    });
    return () => backHandler.remove();
  }, []);

  function renderListHeader() {
    return (
      <View>

        <FeaturedItems data={featuredItems} />

        <HomeTitles title={Localize.t('screens.products')} />
        <View style={{
          height: boxHight.larg,
          // marginStart: wp(5)
        }}>
          <SrcollGroup
            selectedIndex={index1}
            items={[
              'All',
              'IWC',
              'PATEK',
              'PHILIPPE',
              'PANERAI',
              'PATEK PHILIPPE',
            ]}
            onChange={(index: any) => {
              setIndex1(index);
            }}
          />
        </View>
        <View style={{
          height: boxHight.larg,
          // marginStart: wp(5)
        }}>
          <SrcollGroup
            selectedIndex={index2}
            items={[
              'All',
              'PANERAI',
              'PATEK PHILIPPE',
              'IWC',
              'PATEK',
              'PHILIPPE',
            ]}
            onChange={(index: any) => {
              setIndex2(index);
            }}
          />
        </View>

        <HomeTitles title={Localize.t('home.productsHome')} />
        <Block style={{ marginTop: boxHight.small }} />
      </View>
    )
  }
  return (
    <Layout didFinishInitialAnimation={didFinishInitialAnimation}>

      <Header>
        <HomeHeaderChildren />
      </Header>


      <ProductsList data={products}
        header={renderListHeader}
      />
      {/* <ScrollView style={{ marginTop: boxHight.normal / 1.5 }}>
       
      </ScrollView> */}
    </Layout>
  );
};

export default HomeScreen;
