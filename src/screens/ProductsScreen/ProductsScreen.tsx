import React, { FC, ReactElement, useState, useEffect } from 'react';
import {
StatusBar,
  BackHandler,
  InteractionManager,
} from 'react-native';
import styles from './ProductsScreenStyles';
import { Layout, useNavigation, Localize } from 'src/all';
import { Block, Header } from 'src/components';
import { products2th } from 'src/constants/Entry';
import ProductsList from 'src/components/lists/ProductsList';
import { boxHight } from 'src/constants/dimensions';
import ProductsHeaderChildren from './ProductsHeaderChildren';
import { ScreensNames } from 'src/ScreensNames';
import { useTheme } from 'src/constants/theme/ThemeProvider';

const ProductsScreen: FC<{}> = (): ReactElement => {
  const navigation = useNavigation()
  const { colors } = useTheme();

  const [click, setClick] = useState(0);
  const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState(
    false,
  );

  const handleBackButtonClick = () => {
    navigation.navigate(ScreensNames.NAVIGATION_HOME_SCREEN)
    return true;
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButtonClick,
    );

    InteractionManager.runAfterInteractions(() => {
      setTimeout(function () {
        setDidFinishInitialAnimation(true);
      }, 1);
    });
    return () => backHandler.remove();
  }, []);
  return (
    <Layout didFinishInitialAnimation={didFinishInitialAnimation}>

      <Header title={Localize.t('screens.products')}>
        <ProductsHeaderChildren />
      </Header>

      <Block style={{ marginTop: boxHight.small }} />
      <ProductsList data={products2th} />
    </Layout>
  );
};

export default ProductsScreen;
