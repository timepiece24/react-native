import React, { FC, ReactElement, useState, useEffect } from 'react';
import { Text, View } from 'react-native';
import { AsyncStorage, IconAntDesign, wp, Localize } from 'src/all';
import { allColors, appStyles } from 'src/constants';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Input } from 'react-native-elements';
import { boxHight } from 'src/constants/dimensions';
import SrcollGroup from 'src/components/lists/SrcollGroup';
import { color } from 'react-native-reanimated';

const ProductsHeaderChildren: FC<{}> = (): ReactElement => {
  const { colors, isDark } = useTheme();
  const [index, setIndex] = useState(0);

  useEffect(() => {}, []);

  const userName = Localize.t('home.UserName');
  return (
    <View
      style={
        {
          // marginBottom: boxHight.normal
        }
      }>
      <View style={{ height: boxHight.larg, backgroundColor: colors.header }}>
        <SrcollGroup
          selectedIndex={index}
          items={[
            'All',
            'IWC',
            'PATEK PHILIPPE',
            'PANERAI',
            'IWC',
            'PATEK PHILIPPE',
          ]}
          onChange={(index: any) => {
            setIndex(index);
          }}
        />
      </View>
    </View>
  );
};

export default ProductsHeaderChildren;
