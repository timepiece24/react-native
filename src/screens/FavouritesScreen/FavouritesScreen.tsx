import React, { FC, ReactElement, useState, useEffect } from 'react';
import {
  Text,
  View,
  Button,
  ScrollView,
  StyleSheet,
  StatusBar,
  BackHandler,
  InteractionManager,
} from 'react-native';
import styles from './FavouritesScreenStyles';
import { Layout, useNavigation, Localize } from 'src/all';
import { Header, SearchBar } from 'src/components';
import { boxHight } from 'src/constants/dimensions';
import { ScreensNames } from 'src/ScreensNames';
import HomeTitles from '../HomeScreen/homeComponents/HomeTitles';
import ProductsList from 'src/components/lists/ProductsList';
import RowProductsList from 'src/components/lists/RowProductsList';
import { products, products2th, products3th } from 'src/constants/Entry';
import HorizontalProductsList from 'src/components/lists/HorizontalProductsList';

const FavouritesScreen: FC<{}> = (): ReactElement => {
  const navigation = useNavigation()

  const [click, setClick] = useState(0);
  const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState(
    false,
  );
  const handleBackButtonClick = () => {
    navigation.navigate(ScreensNames.NAVIGATION_HOME_SCREEN)
    return true;
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButtonClick,
    );

    InteractionManager.runAfterInteractions(() => {
      setTimeout(function () {
        setDidFinishInitialAnimation(true);
      }, 1);
    });
    return () => backHandler.remove();
  }, []);

  function renderListFooter() {
    return (
      <View
      >
        <RowProductsList data={products3th} />

        <HomeTitles title={Localize.t('product.ProductstoLike')} noView />
        <HorizontalProductsList data={products2th} />

      </View>
    )
  }
  return (
    <Layout didFinishInitialAnimation={didFinishInitialAnimation}>
      <Header  >
        <View style={{
          marginBottom: 40,
        }} />
        <SearchBar />
      </Header>
      <View style={{ marginTop: boxHight.normal / 1.5 }} />
      <ProductsList data={products}
        footer={renderListFooter}
      />



    </Layout>
  );
};

export default FavouritesScreen;
