import { StyleSheet, Dimensions, Platform } from 'react-native';

// import {Theme, allColors, AppStyle, typography} from 'src/constants';
import { hp, wp } from 'src/all';
const { width, height } = Dimensions.get('screen');

const thumbMeasure = (width - 48 - 32) / 3;
const cardWidth = width - wp(2);
const cardHeight = height - wp(2);

export default StyleSheet.create({
  container: {
    paddingHorizontal: wp(5),
    paddingVertical: wp(3),
    // backgroundColor: 'red',
  },
  View: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Button: {
    width: 200,
    margin: 20,
  },
});
