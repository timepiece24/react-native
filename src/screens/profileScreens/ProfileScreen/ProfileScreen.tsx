import React, { FC, ReactElement, useState, useEffect } from 'react';
import {
  Text,
  View,
  Button,
  ScrollView,
  StatusBar,
  BackHandler,
  InteractionManager,
} from 'react-native';
import styles from './ProfileScreenStyles';
import { Layout, AsyncStorage, Localize, useNavigation } from 'src/all';
import { Header, TopBar } from 'src/components';
import { ScreensNames } from 'src/ScreensNames';
import { boxHight } from 'src/constants/dimensions';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { allColors } from 'src/constants';
import InfoTab from './profileComponents/InfoTab';
import SettingsTab from './profileComponents/SettingsTab';
import MyPurchasesTab from './profileComponents/MyPurchasesTab';
import ProfileHeader from './profileComponents/ProfileHeader';
const tabs = [
  'info',
  'MyPurchases', //MyWatches
  'Settings'
]
const ProfileScreen: FC<{}> = (): ReactElement => {
  const navigation = useNavigation();
  const { colors } = useTheme();

  const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState(
    false,
  );
  const [tabIndex, setTabIndex] = useState(0);
  const handleBackButtonClick = () => {
    navigation.navigate(ScreensNames.NAVIGATION_HOME_SCREEN);
    return true;
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButtonClick,
    );
    InteractionManager.runAfterInteractions(() => {
      setTimeout(function () {
        setDidFinishInitialAnimation(true);
      }, 1);
    });
    return () => backHandler.remove();
  }, []);

  const _getRenderItemFunction = () => [renderInfo(), renderList(), renderSettings()][
    tabIndex
  ]
  const renderInfo = () => <InfoTab />
  const renderSettings = () => <SettingsTab />
  const renderList = () => <MyPurchasesTab />


  return (
    <Layout didFinishInitialAnimation={didFinishInitialAnimation}>

      <Header  >
        <View style={{ marginBottom: 40, }} />
        <ProfileHeader />
      </Header>

      <View
        style={{
          height: boxHight.larg,
          backgroundColor: allColors.Transparent,
          // elevation: 5,
          marginTop: 40,

        }}>
        <TopBar
          selectedIndex={tabIndex}
          items={tabs}
          onChange={(index: number) => setTabIndex(index)}
        />
      </View>

      {_getRenderItemFunction()}
    </Layout>
  );
};

export default ProfileScreen;
