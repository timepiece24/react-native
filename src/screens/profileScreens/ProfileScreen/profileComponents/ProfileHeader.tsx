import React, { FC, ReactElement, useState, useEffect } from 'react';
import { View, Text, Image } from 'react-native-animatable';
import { IconAntDesign, Localize, wp } from 'src/all';
import { allColors, appStyles, typography } from 'src/constants';
import { border, boxHight, radius } from 'src/constants/dimensions';
import { useTheme } from 'src/constants/theme/ThemeProvider';

const ProfileHeader: FC<{}> = (): ReactElement => {
    const { colors, isDark } = useTheme();

    useEffect(() => { }, []);

    return (
        <View
            style={{
                ...appStyles.searchBarContainer, flexDirection: 'row',
                height: boxHight.normal - 5,
            }}>
            <View style={{
                marginStart: wp(10),
                // borderRadius: radius.maximum,
             
            }} >
                <Image
                    source={{ uri: 'https://i.postimg.cc/7LbtkxB7/profile.png' }}
                    style={{
                        width: wp(20),
                        height: wp(20),
                        borderRadius: radius.maximum,
                        borderWidth: border.thick,
                        borderColor: allColors.WHITE,
                    }}
                />
            </View>
            <View style={{
                marginStart: wp(3),
                marginVertical: wp(2),
                        height: wp(20),



            }}>
                <Text style={{
                    ...typography.headerName,
                    color: allColors.LightGRAY,
                    // marginTop: wp(3)
                }}> User Name</Text>
                <Text style={{
                    ...typography.headerWelcom,
                    marginTop: wp(1),
                }}> user.name@email.com</Text>
            </View>


        </View>
    );
};

export default ProfileHeader;
