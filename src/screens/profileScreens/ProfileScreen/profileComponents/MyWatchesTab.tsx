import React, { FC, ReactElement, useState, useEffect } from 'react';

import {
  Text,
  View,
} from 'react-native-animatable';
import { useTheme } from 'src/constants/theme/ThemeProvider';

import {
  Layout,
  AsyncStorage,
  Localize,
  useNavigation,
  RNRestart,
  useDispatch,
  useSelector,
} from 'src/all';
import { StorageNames } from 'src/StorageNames';
import { saveToStorage } from 'src/utils/StorageMethods';
import { ScreensNames } from 'src/ScreensNames';
import { ScrollView } from 'react-native-gesture-handler';
import { Block, Button } from 'src/components';
import { animation } from 'src/constants';

const MyWatchesTab: FC<{}> = (): ReactElement => {
  const navigation = useNavigation();
  const { colors } = useTheme();

  const dispatch = useDispatch();


  useEffect(() => {

  }, []);


  return (
    <ScrollView>

      <Block motion={animation.zoomIn}>
        <Text>
          MyList.
        </Text>
      </Block>
    </ScrollView>
  );
};

export default MyWatchesTab;
