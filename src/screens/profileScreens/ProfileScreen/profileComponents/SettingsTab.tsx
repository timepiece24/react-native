import React, { FC, ReactElement, useState, useEffect } from 'react';

import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  BackHandler,
  Button,
  InteractionManager,
  I18nManager,
} from 'react-native';
import { Switch } from 'react-native';
import { useTheme } from 'src/constants/theme/ThemeProvider';

import {
  Layout,
  AsyncStorage,
  Localize,
  useNavigation,
  RNRestart,
  useDispatch,
  useSelector,
} from 'src/all';
import { Header } from 'src/components';
import { StorageNames } from 'src/StorageNames';
import { saveToStorage } from 'src/utils/StorageMethods';
import { ScreensNames } from 'src/ScreensNames';
import { typography } from 'src/constants';
import { toggleTheme } from 'state';

const SettingsTab: FC<{}> = (): ReactElement => {
  const navigation = useNavigation();
  const { colors } = useTheme();

  const dispatch = useDispatch();

  const [language, setLanguage] = useState('');

  const { setScheme, isDark } = useTheme();
  const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState(
    false,
  );
  const handleBackButtonClick = () => {
    navigation.navigate(ScreensNames.NAVIGATION_PROFILE_SCREEN);
    return true;
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButtonClick,
    );

    InteractionManager.runAfterInteractions(() => {
      setTimeout(function () {
        setDidFinishInitialAnimation(true);
        AsyncStorage.getItem(StorageNames.lang).then(
          (value: string | null): void => {
            value && setLanguage(value);
          },
        );
      }, 1);
    });
    return () => backHandler.remove();
  }, []);

  const onClick = () => {
    if (language === 'en') {
      saveToStorage(StorageNames.lang, 'ar');

      I18nManager.forceRTL(true);
    } else if (language === 'ar') {
      saveToStorage(StorageNames.lang, 'en');
      I18nManager.forceRTL(false);
    }
    RNRestart.Restart();
  };
  const toggleScheme = () => {
    isDark ? setScheme('light') : setScheme('dark');
    isDark ? dispatch(toggleTheme('light')) : dispatch(toggleTheme('dark'));
  };

  return (
    <Layout didFinishInitialAnimation={didFinishInitialAnimation}>

      <View style={{}}>

        <Button
          title={
            language == 'en' ? Localize.t('arabic') : Localize.t('english')
          }
          onPress={onClick}
        />
        <Switch value={isDark} onValueChange={toggleScheme} />
      </View>
    </Layout>
  );
};

export default SettingsTab;
