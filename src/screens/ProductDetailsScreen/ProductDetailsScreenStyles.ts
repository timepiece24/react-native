import { StyleSheet, Dimensions, Platform } from 'react-native';

// import {Theme, allColors, AppStyle, typography} from 'src/constants';
import { hp, wp } from 'src/all';
import { appStyles } from 'src/constants';
import { border, radius } from 'src/constants/dimensions';
const { width, height } = Dimensions.get('screen');

export default StyleSheet.create({
  container: {
    paddingHorizontal: wp(5),
    paddingVertical: wp(3),
    // backgroundColor: 'red',
  },
  imageStyle: {
    height: wp(100),
    width: wp(100),
    // width: 70,
    // height: 70,
    alignSelf: 'center',
    resizeMode: 'cover',
  },
  actionButtonStyle: {
    width: wp(15),
    height: wp(15),
    marginVertical: 2,
    ...appStyles.CenterContent,
    borderRadius: radius.noRadius,
    elevation: 2
  }
});
