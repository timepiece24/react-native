import React, { FC, ReactElement, useState, useEffect } from 'react';
import { I18nManager, Text } from 'react-native';
import { allColors, appStyles, typography } from 'src/constants';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Block, Button } from 'src/components';
import { Localize, wp } from 'src/all';
import { border } from 'src/constants/dimensions';

interface Props {
  title?: any;
  noView?: boolean;
}

const DetailsButtons: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();

  useEffect(() => {}, []);

  const onClickWhatsapp = () => console.log('whatsapp')
  const onClicBuyNow = () => console.log('BUY NOW')
  return (
    <Block noMotion style={{ marginVertical: wp(5)}}>
      <Button
        title={'CONTACT USING WHATSAPP'}
        textStyle={{
          ...appStyles.textButttonStyle,
        color: allColors.WHITE}}
        style={{
          ...appStyles.buttonStyle,
          backgroundColor: allColors.GOLD,

          marginVertical: 5,
        }}
        onPress={onClickWhatsapp}
      />
      <Button
        title={'Buy Now'}
        textStyle={{
          ...appStyles.textButttonStyle,
          color: colors.cardText
        }}
        style={{
          ...appStyles.buttonStyle,
          backgroundColor: colors.cardBackgroung,

          marginVertical: 5,
        }}
        onPress={onClicBuyNow}
      />
    </Block>
  );
};

export default DetailsButtons;
