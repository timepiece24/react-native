import React, { FC, ReactElement, useState, useEffect } from 'react';
import { Text } from 'react-native-animatable';
import { allColors, appStyles, typography } from 'src/constants';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Block } from 'src/components';
import { Localize, wp } from 'src/all';
import { border } from 'src/constants/dimensions';

interface Props {
  title?: any;
  noView?: boolean;
}

const DetailsDescription: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();

  useEffect(() => { }, []);

  return (
    <Block noMotion style={{ marginTop: wp(5) }}>
      <Block>
        <Text style={{ ...typography.TextBold10, fontSize: 14, lineHeight: 40, color: colors.search }} >
          Description
      </Text>
      </Block>
      <Block>
        <Text style={{ ...typography.TextLight10, fontSize: 12, color: colors.search }} >
          Lorem ipsum dolor sit amet, 20.000 USD adipiscing elit ean commodo ligula eget dolor. Aenean massa. sociis natoque
      </Text>
      </Block>
    </Block>
  );
};

export default DetailsDescription;
