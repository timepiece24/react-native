import React, { FC, ReactElement, useState, useEffect } from 'react';
import { I18nManager, Text } from 'react-native';
import { allColors, appStyles, typography } from 'src/constants';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Block, Button } from 'src/components';
import { Localize, wp } from 'src/all';
import { border } from 'src/constants/dimensions';
import styles from '../ProductDetailsScreenStyles';

interface Props {
  title?: any;
  noView?: boolean;
}

const ReactionButtons: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();

  let [favourite, setFavourite] = useState(false);
  let [cart, setCart] = useState(false);

  useEffect(() => { }, []);

  const onClickFavourite = () => setFavourite(!favourite)
  const onClicAddToCart = () => setCart(!cart)
  return (
    <Block
      center
      noMotion style={{
        marginVertical: wp(5),
        // bottom: 50,

      }}>
      <Button
        withIcon
        iconLeft
        icon={favourite? 'star':'star-outline'}
        iconStyle={{
          color: colors.search,
          fontSize: 40,
          lineHeight: 50,
          marginTop: wp(3)

        }}
        style={[styles.actionButtonStyle, {
          backgroundColor: colors.cardBackgroung,
        }]}

        onPress={onClickFavourite}
      />
      <Button
        withIcon
        iconLeft
        icon={cart ? 'shopping' :'shopping-outline'}
        iconStyle={{
          color: colors.search,
          fontSize: 40,
          lineHeight: 50,
          marginTop: wp(3)


        }}
        style={[styles.actionButtonStyle, {
          backgroundColor: colors.cardBackgroung,
        }]}
        onPress={onClicAddToCart}
      />
    </Block>
  );
};

export default ReactionButtons;
