import React, { FC, ReactElement, useState, useEffect } from 'react';
import {
  View,
  Image,
  Text,
  ImageBackground,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { AsyncStorage, IconAntDesign, wp, Localize } from 'src/all';
import { allColors, appStyles, typography } from 'src/constants';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Block, Button } from 'src/components';

const { width, height } = Dimensions.get('screen');

interface Props {
  data?: any;
}

const ProductImages: FC<Props> = (props): ReactElement => {
  const { colors, isDark } = useTheme();

  const [activeSlide, setActiveSlide] = useState(1);
  const [activeItem, setactiveItem] = useState(1);

  useEffect(() => { }, []);
  function pagination() {
    return (
      <Pagination
        dotsLength={props.data.length}
        dotColor={allColors.GOLD}
        activeDotIndex={activeSlide}
        containerStyle={{
          paddingVertical: 12,
        }}
        dotStyle={{
          marginHorizontal: -4,
        }}
        inactiveDotColor={allColors.PRIMARY}
        inactiveDotOpacity={1}
        inactiveDotScale={0.6}
      // carouselRef={_carousel}
      // tappableDots={!!_carousel}
      />
    );
  }
  function renderCarousel() {
    return (
      <Carousel
        firstItem={activeSlide}
        data={props.data}
        renderItem={(item: any) => {
          return (
            <Image
              source={{ uri: item.item.image }}
              style={styles.ItemSliderImage}
              blurRadius={0.2}
            />
          );
        }}
        sliderWidth={wp(100)}
        itemWidth={wp(100)}
        onSnapToItem={(index): void => setActiveSlide(index)}
      />
    );
  }

  return (
    <View>
      {renderCarousel()}
      {props.data != undefined && pagination()}
    </View>
  );
};

export default ProductImages;
const styles = StyleSheet.create({
  ItemSliderImage: {
    height: wp(100),
    width: wp(100),
    alignSelf: 'center',
    resizeMode: "cover"

  },
  card: {
    height: wp(100),
    width: wp(100),
    elevation: 3,
  },
  dot: {
    backgroundColor: 'transparent',
    //borderColor: colors.Black,
    borderWidth: 1,
    width: 10,
    height: 10,
    borderRadius: 10,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 0,
    marginBottom: '4%',
  },
  dotActive: {
    //backgroundColor: colors.yellow,
    width: 10,
    height: 10,
    borderRadius: 10,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 0,
    marginBottom: '4%',
  },
});
