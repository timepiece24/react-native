import React, { FC, ReactElement, useState, useEffect } from 'react';
import {
  Button,
  ScrollView,
  StatusBar,
  BackHandler,
} from 'react-native';
import styles from './ProductDetailsScreenStyles';
import { Image, View, Text } from 'react-native-animatable'
import { Layout, useNavigation, Localize, wp } from 'src/all';
import { Block, Header } from 'src/components';
import { boxHight } from 'src/constants/dimensions';
import ProductDetailsHeader from './productComponents/ProductDetailsHeader';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { allColors, typography } from 'src/constants';
import DetailsDescription from './productComponents/DetailsDescription';
import DetailsButtons from './productComponents/DetailsButtons';
import { imageslider } from 'src/constants/Entry';
import ProductImages from './productComponents/ProductImages';
import ReactionButtons from './productComponents/ReactionButtons';
import { CommonActions } from '@react-navigation/native';

const ProductDetailsScreen: FC<{}> = (): ReactElement => {
  const navigation = useNavigation()
  const { colors, isDark } = useTheme();
  const handleBackButtonClick = () => {
    navigation.dispatch(CommonActions.goBack());
    return true;
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButtonClick,
    );

    return () => backHandler.remove();
  }, []);
  return (
    // <Layout didFinishInitialAnimation={didFinishInitialAnimation} noStatus>
    <View style={{ backgroundColor: colors.background, height: '100%' }}  >

      <StatusBar barStyle={'light-content'} backgroundColor={allColors.Transparent} translucent={true} />

      <ProductDetailsHeader title={Localize.t('product.itemDetails')} />


      <ProductImages data={imageslider} />

      <Block style={{
        position: 'absolute',
        top: wp(56),
        left: wp(82),
        zIndex: 1000

      }}>
        <ReactionButtons />

      </Block>

      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ paddingHorizontal: wp(5), }} >
        <Block style={{ marginTop: boxHight.small }} />

        <Block center>
          <Text numberOfLines={2} style={{ ...typography.TextLight10, fontSize: 18, lineHeight: 30, color: colors.search, width: wp(60), textAlign: 'center' }} >
            Watch full Name
          </Text>
          <Text numberOfLines={2} style={{ ...typography.TextLight10, fontSize: 18, lineHeight: 30, color: colors.search, width: wp(60), textAlign: 'center' }} >
            Brand Name
          </Text>

        </Block>

        <Block center>
          <Text numberOfLines={2} style={{ ...typography.TextExtraBold12, fontSize: 18, lineHeight: 60, color: colors.price }} >
            16.000 $$
          </Text>
        </Block>

        <DetailsDescription />

        <DetailsButtons />
      </ScrollView>
    </View>
    // </Layout>
  );
};

export default ProductDetailsScreen;
