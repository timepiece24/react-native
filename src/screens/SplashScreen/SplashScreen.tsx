import React, { FC, ReactElement } from 'react';
import { View, StatusBar, Dimensions } from 'react-native';
import { Image } from 'react-native-animatable';

import styles from './SplashScreenStyles';
import { hp, Loader, wp, IconLogo } from 'src/all';
import { allColors, animation, Images } from 'src/constants';
import { Block } from 'src/components';

const { width, height } = Dimensions.get('screen');

const SplashScreen: FC<{}> = (): ReactElement => {
  return (
    <View style={{ width: width, height: height }}>
      <StatusBar
        animated
        barStyle={'light-content'}
        backgroundColor={allColors.DARKBLUE}
      />

      <View style={styles.container}>
        <Block center>
          <Image
            animation={animation.bounceIn}
            duration={4500}
            delay={0}
            source={Images.logo}
            style={styles.logo}
          />
        </Block>
        {/* <IconLogo width={wp(80)} height={hp(10)} /> */}
        {/* <Loader didFinishInitialAnimation={false} /> */}
      </View>
    </View>
  );
};

export default SplashScreen;
