import { StyleSheet } from 'react-native';
import { wp } from 'src/all';
import { allColors, appStyles } from 'src/constants';

export default StyleSheet.create({
  container: {
    paddingHorizontal: wp(5),
    paddingVertical: wp(60),
    backgroundColor: allColors.DARKBLUE,
    width: '100%',
    height: '100%',
  },
  logo: {
    ...appStyles.logo,
  },
});
