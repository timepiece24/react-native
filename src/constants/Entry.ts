export const imageslider = [
  {
    id: '1',
    image: 'https://i.postimg.cc/5yM6hzCy/accessory-stylish-time-time-piece-3x.png',
  },
  {
    id: '2',
    image: 'https://i.postimg.cc/5yM6hzCy/accessory-stylish-time-time-piece-3x.png',
  },
  {
    id: '3',
    image: 'https://i.postimg.cc/5yM6hzCy/accessory-stylish-time-time-piece-3x.png',
  },
  {
    id: '4',
    image: 'https://i.postimg.cc/5yM6hzCy/accessory-stylish-time-time-piece-3x.png',
  },
];
export const featuredItems = [
  {
    id: '1',
    title: 'CHOPARD WATCH',
    image: 'https://i.postimg.cc/sD4NWd7w/wp1853720-3x.png',
  },
  {
    id: '2',
    title: 'CHOPARD WATCH',
    image: 'https://i.postimg.cc/QMw4DXZx/Chopard-Mille-Miglia-LG-3x.png',
  },
  {
    id: '3',
    title: 'CHOPARD WATCH',
    image: 'https://i.postimg.cc/sD4NWd7w/wp1853720-3x.png',
  },
  {
    id: '4',
    title: 'CHOPARD WATCH',
    image: 'https://i.postimg.cc/QMw4DXZx/Chopard-Mille-Miglia-LG-3x.png',
  },
];
export const products = [
  {
    id: '1',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/4dvvmcSy/Daco-4419792-3x.png',
  },
  {
    id: '2',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/jjm3zych/22540-9-branded-watch-clipart-3x.png',
  },
  {
    id: '3',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/FHfj08B2/unnamed-3x.png',
  },
  {
    id: '4',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/WbYGFTXG/unnamed-2-3x.png',
  },
];

export const products2th = [
  {
    id: '1',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/4dvvmcSy/Daco-4419792-3x.png',
  },
  {
    id: '2',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/jjm3zych/22540-9-branded-watch-clipart-3x.png',
  },
  {
    id: '3',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/FHfj08B2/unnamed-3x.png',
  },
  {
    id: '4',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/WbYGFTXG/unnamed-2-3x.png',
  },
  {
    id: '5',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/4dvvmcSy/Daco-4419792-3x.png',
  },
  {
    id: '6',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/jjm3zych/22540-9-branded-watch-clipart-3x.png',
  },
  {
    id: '7',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/FHfj08B2/unnamed-3x.png',
  },
  {
    id: '8',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/WbYGFTXG/unnamed-2-3x.png',
  },
];

export const products3th = [
  {
    id: '1',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/4dvvmcSy/Daco-4419792-3x.png',
  },
  {
    id: '2',
    title: 'Brand Name',
    price: '17.560 $',
    image: 'https://i.postimg.cc/jjm3zych/22540-9-branded-watch-clipart-3x.png',
  },

];
