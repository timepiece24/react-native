import * as React from 'react';
import { useColorScheme } from 'react-native-appearance';
import { setCurrentTheme } from 'state';
import { lightColors, darkColors, Colors } from './colorThemes';
import { useSelector } from 'react-redux';

export interface Theme {
  isDark: boolean;
  colors: Colors;
  setScheme: (val: 'dark' | 'light') => void;
}

export const ThemeContext = React.createContext<Theme>({
  isDark: false,
  colors: lightColors,
  setScheme: () => {},
});

export const ThemeProvider: React.FC<{}> = (props) => {
  const currentTheme = useSelector(setCurrentTheme);
  const colorScheme = useColorScheme();

  const [isDark, setIsDark] = React.useState<boolean>(colorScheme === 'dark');

  React.useEffect(() => {
    setIsDark(
      currentTheme === 'dark' ? true : colorScheme === 'dark' ? true : false,
    );
  }, [colorScheme]);

  const defaultTheme: Theme = {
    isDark,
    colors: isDark ? darkColors : lightColors,
    setScheme: (scheme) => setIsDark(scheme === 'dark'),
  };

  return (
    <ThemeContext.Provider value={defaultTheme}>
      {props.children}
    </ThemeContext.Provider>
  );
};

export const useTheme = () => React.useContext(ThemeContext);
