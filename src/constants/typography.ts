import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Platform } from 'react-native';

//

const REGULAR = 'Poppins-Regular';
const BOLD = 'Poppins-Bold';
const SEMIBOLD = 'Poppins-Bold';
const EXTRABOLD = 'Poppins-ExtraBold';
const MEDIUM = 'Poppins-Medium';
const PRIMARY = 'Poppins-Light';

import { I18nManager } from 'react-native';
import allColors from './allColors';
import { border, boxHight } from './dimensions';

export default {
  headerName: {
    fontSize: 22,
    lineHeight: 30,
    fontFamily: MEDIUM,
    textAlign: 'left',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  viewAll: {
    fontSize: 12,
    lineHeight: 20,
    fontFamily: MEDIUM,
    textAlign: 'left',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
    color: allColors.GOLD,

    borderWidth: border.noborder,
    borderBottomWidth: border.thick,
    borderColor: allColors.GOLD,
    marginStart: wp(20),
  },
  headerWelcom: {
    fontSize: 14,
    lineHeight: 23,
    fontFamily: PRIMARY,
    textAlign: 'left',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },

  TextInput: {
    fontSize: 14,
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: I18nManager.isRTL ? 'right' : 'left',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  ButtonText: {
    fontSize: 16,
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  authQuestion: {
    fontSize: 14,
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextBold10: {
    fontSize: wp('3%'),
    fontFamily: I18nManager.isRTL ? BOLD : BOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextBold11: {
    fontSize: wp('3.5%'),
    fontFamily: I18nManager.isRTL ? BOLD : BOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextBold12: {
    fontSize: wp('4%'),
    fontFamily: I18nManager.isRTL ? BOLD : BOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextBold13: {
    fontSize: wp('4.5%'),
    fontFamily: I18nManager.isRTL ? BOLD : BOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextBold14: {
    fontSize: wp('5%'),
    fontFamily: I18nManager.isRTL ? BOLD : BOLD,
    textAlign: I18nManager.isRTL ? 'center' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextBold16: {
    fontSize: wp('6%'),
    fontFamily: I18nManager.isRTL ? BOLD : BOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextBold18: {
    fontSize: wp('7%'),
    fontFamily: I18nManager.isRTL ? BOLD : BOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraBold12: {
    fontSize: wp('4%'),
    fontFamily: I18nManager.isRTL ? EXTRABOLD : EXTRABOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraBold13: {
    fontSize: wp('4.5%'),
    fontFamily: I18nManager.isRTL ? EXTRABOLD : EXTRABOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraBold14: {
    fontSize: wp('5%'),
    fontFamily: I18nManager.isRTL ? EXTRABOLD : EXTRABOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraBold16: {
    fontSize: wp('6%'),
    fontFamily: I18nManager.isRTL ? EXTRABOLD : EXTRABOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraBold18: {
    fontSize: wp('7%'),
    fontFamily: I18nManager.isRTL ? EXTRABOLD : EXTRABOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraBold20: {
    fontSize: wp('8%'),
    fontFamily: I18nManager.isRTL ? EXTRABOLD : EXTRABOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraBold22: {
    fontSize: wp('9%'),
    fontFamily: I18nManager.isRTL ? EXTRABOLD : EXTRABOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextBlack: {
    fontSize: wp('4%'),
    fontFamily: I18nManager.isRTL ? SEMIBOLD : SEMIBOLD,
    textAlign: I18nManager.isRTL ? 'right' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },

  TextInputHelp: {
    fontSize: wp('3%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'right' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
    // fontStyle: 'italic',
  },
  TextExtraLight6: {
    fontSize: wp('1%'),
    fontFamily: I18nManager.isRTL ? PRIMARY : PRIMARY,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraLight8: {
    fontSize: wp('2%'),
    fontFamily: I18nManager.isRTL ? PRIMARY : PRIMARY,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraLight10: {
    fontSize: wp('3%'),
    fontFamily: I18nManager.isRTL ? PRIMARY : PRIMARY,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraLight12: {
    fontSize: wp('4%'),
    fontFamily: I18nManager.isRTL ? PRIMARY : PRIMARY,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraLight14: {
    fontSize: wp('5%'),
    fontFamily: I18nManager.isRTL ? PRIMARY : PRIMARY,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraLight16: {
    fontSize: wp('6%'),
    fontFamily: I18nManager.isRTL ? PRIMARY : PRIMARY,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraLight18: {
    fontSize: wp('7%'),
    fontFamily: I18nManager.isRTL ? PRIMARY : PRIMARY,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextExtraLight20: {
    fontSize: wp('8%'),
    fontFamily: I18nManager.isRTL ? PRIMARY : PRIMARY,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight6: {
    fontSize: wp('1%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight8: {
    fontSize: wp('2%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight10: {
    fontSize: wp('3%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'right' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight11: {
    fontSize: wp('3.5%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight12: {
    fontSize: wp('4%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight13: {
    fontSize: wp('4.5%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight14: {
    fontSize: wp('5%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight16: {
    fontSize: wp('6%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight18: {
    fontSize: wp('7%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextLight20: {
    fontSize: wp('8%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextMedium: {
    fontSize: wp('4%'),
    fontFamily: I18nManager.isRTL ? MEDIUM : MEDIUM,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextHeader: {
    fontSize: wp('6%'),
    fontFamily: I18nManager.isRTL ? SEMIBOLD : SEMIBOLD,
    textAlign: I18nManager.isRTL ? 'left' : 'left',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextRegular10: {
    fontSize: wp('3%'),
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextRegular11: {
    fontSize: wp('3.5%'),
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: I18nManager.isRTL ? 'right' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextRegular12: {
    fontSize: wp('4%'),
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextRegular13: {
    fontSize: wp('4.5%'),
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextRegular14: {
    fontSize: wp('5%'),
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextRegular16: {
    fontSize: wp('6%'),
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
  TextRegular18: {
    fontSize: wp('7%'),
    fontFamily: I18nManager.isRTL ? REGULAR : REGULAR,
    textAlign: I18nManager.isRTL ? 'left' : 'auto',
    paddingTop: Platform.OS === 'ios' ? wp(1) : 0,
  },
};
