import { StyleProp, TextStyle } from 'react-native';

import { StackNavigationProp } from '@react-navigation/stack';

type StackParamList = {
  default: undefined;
  LoginScreen: undefined;
  RegisterScreen: undefined;
  StackNavigator: undefined;
  BottomTabNavigator: undefined;
};

export type DefaultNavigationProps<
  T extends keyof StackParamList
> = StackNavigationProp<StackParamList, T>;
