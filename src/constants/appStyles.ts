import { I18nManager, Platform, StyleSheet } from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import allColors from './allColors';
import typography from './typography';

import { border, radius, boxHight } from './dimensions';
export default {
  inputStyle: {
    borderRadius: radius.noRadius,
    borderColor: allColors.WHITE,
    backgroundColor: allColors.DARKBLUE,
    borderWidth: border.thin,
    height: boxHight.normal,
    width: wp(90),
    elevation: 5,
    ...typography.TextRegular10,
    fontSize: 10,
    color: allColors.WHITE,
    marginVertical: 20,
  },
  textInputStyle: {
    ...typography.TextInput,
    lineHeight: wp('4%'),
    color: allColors.WHITE,
    marginTop: wp(2),
    marginHorizontal: 2,
  },
  errorInputStyle: {
    color: allColors.MUTED,
    bottom: wp(2),
  },
  buttonStyle: {
    justifyContent: 'center',
    borderRadius: radius.noRadius,
    borderColor: allColors.WHITE,
    backgroundColor: allColors.WHITE,
    borderWidth: border.noborder,
    height: boxHight.normal,
    width: wp(90),
    elevation: 5,
    marginVertical: 20,
    // paddingTop: wp(1),
  },
  logo: {
    width: wp(55),
    height: hp(11),
  },
  textSearchBarStyle: {
    ...typography.TextInput,
    lineHeight: wp('4%'),
    color: allColors.MUTED,
    marginTop: wp(2),
    marginHorizontal: 14,
  },
  homeTitlesBlock: {
    marginTop: hp(2),
    // marginStart: wp(5),
    flexDirection: 'row',
  },
  searchBarContainer: {
    position: 'absolute',
    height: boxHight.normal - 5,
    bottom: boxHight.verySmall,
    // marginTop: 40,

  },

  searchBarStyle: {
    borderRadius: radius.noRadius,
    height: boxHight.normal,
    width: wp(95),
    elevation: 5,
    ...typography.TextRegular10,
    fontSize: 10,
    color: allColors.WHITE,
    marginTop: 20,
    borderColor: allColors.Transparent,

  },
  textButttonStyle: {
    ...typography.ButtonText,
    lineHeight: wp('4.5%'),
    color: allColors.DARKBLUE,
    marginTop: wp(2),
    marginHorizontal: 13,
  },
  listStyle: {
    backgroundColor: allColors.Transparent,

    paddingHorizontal: wp(5),
    // width: wp(95),
  },
  CenterContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  CenterContentRow: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  CenterBottomContent: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  CenterBottomLeftContent: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  CenterBottomRightContent: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  CenterLeftContent: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  CenterRightContent: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  CenterTopContent: {
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  CenterRightTopContent: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  CenterText: {
    textAlign: 'center',
  },
};
