export const border = {
  noborder: 0,
  thin: 1,
  thick: 2,
};

export const radius = {
  noRadius: 1,
  minmum: 15,
  medium: 30,
  maximum: 60,
};
export const boxHight = {
  verySmall: 1,
  small: 14,
  normal: 44,
  larg: 60,
  bottomBar: 60,
};
