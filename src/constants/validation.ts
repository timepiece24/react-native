import * as yup from 'yup';
import { Localize } from 'src/all';

const loginSchema = yup.object().shape({
  username: yup.string().required(Localize.t('validation.username')),
  password: yup.string().required(Localize.t('validation.password')),
});

const registerSchema = yup.object().shape({
  username: yup.string().required(Localize.t('validation.username')),
  password: yup.string().required(Localize.t('validation.password')),
  confirmPassword: yup
    .string()
    .required(Localize.t('validation.confirmPassword')),
  phoneNumber: yup.string().required(Localize.t('validation.mobile')),
  email: yup.string().required(Localize.t('validation.email')),
  address: yup.string().required(Localize.t('validation.address')),
});

export { loginSchema, registerSchema };
