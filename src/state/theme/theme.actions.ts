import { createAsyncThunk } from '@reduxjs/toolkit';

export const toggleTheme = createAsyncThunk<any, any, any>(
  '',
  async (args: any, { rejectWithValue }) => {
    try {
      return args;
    } catch (e) {
      return rejectWithValue(e);
    }
  },
);
