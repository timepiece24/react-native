import { createSlice } from '@reduxjs/toolkit';
import { toggleTheme } from './theme.actions';
import { RootState } from 'state/store';

type sliceState = {
  currentTheme: string | null | undefined;
};

const initialState: sliceState = {
  currentTheme: null,
};

export const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(toggleTheme.fulfilled, (state, action) => {
      const theme = action.payload;
      state.currentTheme = theme;
    });
  },
});

export const setCurrentTheme = (state: RootState) => state.theme.currentTheme;

export default themeSlice.reducer;
