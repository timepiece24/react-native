import { applyMiddleware, createStore, compose, combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import Thunk from 'redux-thunk';

import userSlice from './user/user.slice';
import themeSlice from './theme/theme.slice';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['user', 'theme'],
};

const rootReducer = combineReducers({
  user: userSlice,
  theme: themeSlice,
});

export type RootState = ReturnType<typeof rootReducer>;
const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, compose(applyMiddleware(Thunk)));

const persistor = persistStore(store);
// persistor.purge();

export { store, persistor };
