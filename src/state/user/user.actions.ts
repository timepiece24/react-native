import { createAsyncThunk } from '@reduxjs/toolkit';

export const loginUser = createAsyncThunk<any, any, any>(
  'auth/login',
  async (args: any, { rejectWithValue }) => {
    try {
      //   const response: any = await authApi.login(args);
      //   return response.data;

      // Mocking api response by returning the args immediately
      console.log('args', args);
      return args;
    } catch (e) {
      return rejectWithValue(e);
    }
  },
);

export const registerUser = createAsyncThunk<any, any, any>(
  'auth/register',
  async (args: any, { rejectWithValue }) => {
    try {
      //   const response: any = await authApi.register(args);
      //   return response.data;
    } catch (e) {
      return rejectWithValue(e);
    }
  },
);

export const resetPassword = createAsyncThunk<any, any, any>(
  'auth/reset',
  async (args: any, { rejectWithValue }) => {
    try {
      //   const response: any = await authApi.resetPassword(args);
      //   return response.data;
    } catch (e) {
      return rejectWithValue(e);
    }
  },
);
