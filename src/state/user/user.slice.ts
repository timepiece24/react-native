import { createSlice } from '@reduxjs/toolkit';
import { loginUser, registerUser } from './user.actions';
import { RootState } from 'state/store';

type sliceState = {
  user: {
    id: string | undefined;
    name: string | undefined;
    username: string | undefined;
    password: string | undefined;
    phone: string | undefined;
  };
  loading: boolean;
  accessToken: string | undefined;
  isAuthenticated: boolean;
  error: string | undefined;
};

const initialState: sliceState = {
  user: {
    id: undefined,
    name: undefined,
    username: undefined,
    password: undefined,
    phone: undefined,
  },

  loading: false,
  accessToken: undefined,
  isAuthenticated: false,
  error: undefined,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logout: () => initialState,
  },
  extraReducers: (builder) => {
    builder.addCase(loginUser.fulfilled, (state, action) => {
      const { user, Authorization } = action.payload;
      state.user = user;
      state.loading = false;
      state.isAuthenticated = true;
      state.accessToken = Authorization;
    });

    builder.addCase(loginUser.pending, (state) => {
      state.loading = true;
      state.error = undefined;
    });

    builder.addCase(loginUser.rejected, (state, action) => {
      // @ts-ignore
      const { message } = action.payload;
      state.error = message?.ar;
      state.loading = false;
    });

    builder.addCase(registerUser.fulfilled, (state, action) => {
      const { user, Authorization } = action.payload;
      state.user = user;
      state.loading = false;
      state.accessToken = Authorization;
    });

    builder.addCase(registerUser.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(registerUser.rejected, (state, action) => {
      // @ts-ignore
      const { message } = action.payload;
      state.error = message.ar;
      state.loading = false;
    });
  },
});

export const { logout } = userSlice.actions;

export const selectUser = (state: RootState) => state.user.user;
export const selectError = (state: RootState) => state.user.error;
export const selectLoading = (state: RootState) => state.user.loading;
export const selectToken = (state: RootState) => state.user.accessToken;
export const selectAuthentication = (state: RootState) =>
  state.user.isAuthenticated;

export default userSlice.reducer;
