const lang = 'lang';

const access_token = 'access_token';
const token_type = 'token_type';

const welcome = 'welcome';
const userType = 'userType';

export const StorageNames = {
  lang,
  access_token,
  token_type,
  welcome,
  userType,
};
