import {
  BottomTabNavigationProp,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import React, { ReactElement } from 'react';

import { allColors, Images, typography } from 'src/constants';
import { Image } from 'react-native';

import { ScreensObject } from 'src/screens';
import { ScreensNames } from 'src/ScreensNames';
import StackNavigator from './HomeStack';
import ProfileStack from './ProfileStack';
import {
  IconAntMaterialCommunityIcons,
  IconAntIonicons,
  IconWatch,
  Localize,
  wp,
} from 'src/all';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Text } from 'react-native-animatable';
import { Block } from 'src/components';
import { border, boxHight } from 'src/constants/dimensions';

export type BottomTabParamList = {
  default: undefined;
};
export type BottomTabNavigationProps<
  T extends keyof BottomTabParamList = 'default'
> = BottomTabNavigationProp<BottomTabParamList, T>;

// const Tab = createBottomTabNavigator<BottomTabParamList>();

const Tab = createBottomTabNavigator();

function TabBarIcon(arg: {
  focused: boolean;
  iconName: string;
}): React.ReactElement {
  const { colors, isDark } = useTheme();
  if (arg.iconName == 'products') {
    return (
      <IconWatch
        style={{
          color: arg.focused ? allColors.GOLD : colors.bottomBar,
          backgroundColor: colors.cardBackgroung,
          fontSize: arg.focused ? 25 : 18,
        }}
      />
    );
  } else
    return (
      <IconAntIonicons
        name={arg.iconName}
        style={{
          color: arg.focused ? allColors.GOLD : colors.bottomBar,

          fontSize: arg.focused ? 25 : 18,
        }}
      />
    );
}
function TabBarTitle(arg: {
  focused: boolean;
  title: string;
}): React.ReactElement {
  const { colors, isDark } = useTheme();
  return (
    <Text
      style={{
        ...typography.TextLight10,
        fontSize: arg.focused ? 13 : 12,
        textAlign: 'center',
        marginTop: 5,
        color: arg.focused ? allColors.GOLD : colors.bottomBar,
      }}>
      {' '}
      {arg.title}{' '}
    </Text>
  );
}

function renderLabel(arg: {
  focused: boolean;
  title: string;
  iconName: string;
}): React.ReactElement {
  const { colors, isDark } = useTheme();

  if (arg.iconName == 'products') {
    return (
      <Block
        center
        noMotion
        style={{
          backgroundColor: colors.cardBackgroung,
          paddinVertical: 3,
          width: wp(40),
          height: boxHight.bottomBar,
        }}>
        <IconWatch
          style={{
            color: arg.focused ? allColors.GOLD : colors.bottomBar,
            fontSize: arg.focused ? 25 : 18,
            alignSelf: 'center',
            height: boxHight.normal,
          }}
        />
        {TabBarTitle({ focused: arg.focused, title: arg.title })}
      </Block>
    );
  } else
    return (
      <Block
        noMotion
        center
        style={{
          paddinVertical: 3,
          backgroundColor: colors.cardBackgroung,
          width: wp(40),
          height: boxHight.bottomBar,
        }}>
        <IconAntIonicons
          name={arg.iconName}
          style={{
            color: arg.focused ? allColors.GOLD : colors.bottomBar,
            textAlign: 'center',
            // height: boxHight.normal,
            fontSize: arg.focused ? 25 : 18,
          }}
        />
        {TabBarTitle({ focused: arg.focused, title: arg.title })}
      </Block>
    );
}

function BottomTabNavigator(): ReactElement {
  const { colors, isDark } = useTheme();

  return (
    <Tab.Navigator>
      <Tab.Screen
        name={ScreensNames.NAVIGATION_HOME_SCREEN}
        component={StackNavigator}
        options={{
          tabBarLabel: ({ focused }): React.ReactElement =>
            renderLabel({
              focused: focused,
              title: Localize.t('screens.home'),
              iconName: focused == true ? 'md-home' : 'md-home-outline',
            }),

          // tabBarIcon: ({ focused }): React.ReactElement =>
          //   TabBarIcon({ focused: focused, iconName: focused == true ? 'md-home' : 'md-home-outline' }),
        }}
      />
      <Tab.Screen
        name={ScreensNames.NAVIGATION_PRODUCTS_SCREEN}
        component={ScreensObject.ProductsScreen}
        options={{
          tabBarLabel: ({ focused }): React.ReactElement =>
            renderLabel({
              focused: focused,
              title: Localize.t('screens.watch'),
              iconName: 'products',
            }),
          // tabBarIcon: ({ focused }): React.ReactElement =>
          //   TabBarIcon({ focused: focused, iconName: 'products' }),
        }}
      />
      <Tab.Screen
        name={ScreensNames.NAVIGATION_FAVOURITES_SCREEN}
        component={ScreensObject.FavouritesScreen}
        options={{
          tabBarLabel: ({ focused }): React.ReactElement =>
            renderLabel({
              focused: focused,
              title: Localize.t('screens.favourites'),
              iconName: focused == true ? 'star' : 'star-outline',
            }),
          // tabBarIcon: ({ focused }): React.ReactElement =>
          //   TabBarIcon({ focused: focused, iconName: focused == true ? 'star' : 'star-outline' }),
        }}
      />
      <Tab.Screen
        name={ScreensNames.NAVIGATION_PROFILE_SCREEN}
        component={ProfileStack}
        options={{
          tabBarLabel: ({ focused }): React.ReactElement =>
            renderLabel({
              focused: focused,
              title: Localize.t('screens.profile'),
              iconName: focused == true ? 'person' : 'person-outline',
            }),
          // tabBarIcon: ({ focused }): React.ReactElement =>
          //   TabBarIcon({ focused: focused, iconName: focused == true ? 'person' : 'person-outline' }),
        }}
      />
    </Tab.Navigator>
  );
}

export default BottomTabNavigator;
