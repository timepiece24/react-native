import {
  BottomTabNavigationProp,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import React, { ReactElement } from 'react';

import { allColors, Images, typography } from 'src/constants';
import { Image } from 'react-native';

import { ScreensObject } from 'src/screens';
import { ScreensNames } from 'src/ScreensNames';
import StackNavigator from './HomeStack';
import ProfileStack from './ProfileStack';
import ProductStack from './ProductStack';
import {
  IconAntMaterialCommunityIcons,
  IconAntIonicons,
  IconWatch,
  Localize,
} from 'src/all';
import { useTheme } from 'src/constants/theme/ThemeProvider';
import { Text } from 'react-native-animatable';
import { Block } from 'src/components';
import FavouritesStack from './FavouritesStack';

export type BottomTabParamList = {
  default: undefined;
};
export type BottomTabNavigationProps<
  T extends keyof BottomTabParamList = 'default'
> = BottomTabNavigationProp<BottomTabParamList, T>;

// const Tab = createBottomTabNavigator<BottomTabParamList>();

const Tab = createBottomTabNavigator();

function TabBarIcon(arg: {
  focused: boolean;
  iconName: string;
}): React.ReactElement {
  const { colors, isDark } = useTheme();
  if (arg.iconName == 'products') {
    return (
      <IconWatch
        fill= {arg.focused ?allColors.GOLD : colors.bottomBar}

        style={{
          fontSize: arg.focused ? 25 : 18,
          fontWeight: 'bold',
        }}
      />
    );
  } else
    return (
      <IconAntIonicons
        name={arg.iconName}
        style={{
          color: arg.focused ? allColors.GOLD : colors.bottomBar,

          fontSize: arg.focused ? 25 : 18,
        }}
      />
    );
}
function TabBarTitle(arg: {
  focused: boolean;
  title: string;
}): React.ReactElement {
  const { colors, isDark } = useTheme();
  return (
    <Text
      style={{
        ...typography.TextLight10,
        fontSize: arg.focused ? 13 : 12,
        color: arg.focused ? allColors.GOLD : colors.bottomBar,
      }}>
      {' '}
      {arg.title}{' '}
    </Text>
  );
}

function BottomTabNavigator(): ReactElement {
  const { colors, isDark } = useTheme();

  return (
    <Tab.Navigator>
      <Tab.Screen
        name={ScreensNames.NAVIGATION_HOME_SCREEN}
        component={StackNavigator}
        options={{
          tabBarLabel: ({ focused }): React.ReactElement =>
            TabBarTitle({
              focused: focused,
              title: Localize.t('screens.home'),
            }),

          tabBarIcon: ({ focused }): React.ReactElement =>
            TabBarIcon({
              focused: focused,
              iconName: focused == true ? 'md-home' : 'md-home-outline',
            }),
        }}
      />
      <Tab.Screen
        name={ScreensNames.NAVIGATION_PRODUCTS_SCREEN}
        component={ProductStack}
        options={{
          tabBarLabel: ({ focused }): React.ReactElement =>
            TabBarTitle({
              focused: focused,
              title: Localize.t('screens.watch'),
            }),
          tabBarIcon: ({ focused }): React.ReactElement =>
            TabBarIcon({ focused: focused, iconName: 'products' }),
        }}
      />
      <Tab.Screen
        name={ScreensNames.NAVIGATION_FAVOURITES_SCREEN}
        component={FavouritesStack}
        options={{
          tabBarLabel: ({ focused }): React.ReactElement =>
            TabBarTitle({
              focused: focused,
              title: Localize.t('screens.favourites'),
            }),
          tabBarIcon: ({ focused }): React.ReactElement =>
            TabBarIcon({
              focused: focused,
              iconName: focused == true ? 'star' : 'star-outline',
            }),
        }}
      />
      <Tab.Screen
        name={ScreensNames.NAVIGATION_PROFILE_SCREEN}
        component={ProfileStack}
        options={{
          tabBarLabel: ({ focused }): React.ReactElement =>
            TabBarTitle({
              focused: focused,
              title: Localize.t('screens.profile'),
            }),
          tabBarIcon: ({ focused }): React.ReactElement =>
            TabBarIcon({
              focused: focused,
              iconName: focused == true ? 'person' : 'person-outline',
            }),
        }}
      />
    </Tab.Navigator>
  );
}

export default BottomTabNavigator;
