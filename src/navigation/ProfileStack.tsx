import React from 'react';
import { ScreensNames } from 'src/ScreensNames';
import { ScreensObject } from 'src/screens';

import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

function ProfileStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        transitionSpec: {
          duration: 100,
          useNativeDriver: true,
        },
      }}>
      <Stack.Screen
        name={ScreensNames.NAVIGATION_PROFILE_SCREEN}
        component={ScreensObject.ProfileScreen}
      />
      <Stack.Screen
        name={ScreensNames.NAVIGATION_SETTINGS_SCREEN}
        component={ScreensObject.SettingsScreen}
      />
    </Stack.Navigator>
  );
}

export default ProfileStack;
