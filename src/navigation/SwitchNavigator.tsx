import {
  NativeStackNavigationProp,
  createNativeStackNavigator,
} from '@react-navigation/native-stack';

import BottomTabNavigator from './BottomTabNavigator';
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from '@react-navigation/native';
import React from 'react';
import HomeStack from './HomeStack';
import { AppearanceProvider, useColorScheme } from 'react-native-appearance';
import { useTheme } from 'src/constants/theme/ThemeProvider';

import { ScreensObject } from 'src/screens';
import { ScreensNames } from 'src/ScreensNames';

// import DarkThem from 'src/constants/theme/colorThemes';
// import LightTheme from 'src/constants/theme/colorThemes';

const Stack = createNativeStackNavigator();

function SwitchNavigator(): React.ReactElement {
  const { colors, isDark } = useTheme();
  return (
    <NavigationContainer
    // theme={isDark == 'dark' ? DarkThem : LightTheme}
    >
      <Stack.Navigator
        mode="modal"
        screenOptions={{
          headerShown: false,
          gestureEnabled: true,
          transitionSpec: {
            duration: 100,
            useNativeDriver: true,
          },
        }}>
        <Stack.Screen
          name={ScreensNames.NAVIGATION_LOGIN_SCREEN}
          component={ScreensObject.LoginScreen}
        />
        <Stack.Screen
          name={ScreensNames.NAVIGATION_REGISTER_SCREEN}
          component={ScreensObject.RegisterScreen}
        />
        <Stack.Screen
          name={ScreensNames.NAVIGATION_CONFIRMATION_CODE_SCREEN}
          component={ScreensObject.ConfirmationCodeScreen}
        />

        {/* <Stack.Screen name="HomeStack" component={HomeStack} /> */}
        <Stack.Screen
          name="BottomTabNavigator"
          component={BottomTabNavigator}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default SwitchNavigator;
