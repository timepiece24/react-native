import React from 'react';
import { ScreensNames } from 'src/ScreensNames';
import { ScreensObject } from 'src/screens';

import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

function ProductStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        transitionSpec: {
          duration: 100,
          useNativeDriver: true,
        },
      }}>
      <Stack.Screen
        name={ScreensNames.NAVIGATION_PRODUCTS_SCREEN}
        component={ScreensObject.ProductsScreen}
      />
      <Stack.Screen
        name={ScreensNames.NAVIGATION_PRODUCT_DETAILS_SCREEN}
        component={ScreensObject.ProductDetailsScreen}
      />
    </Stack.Navigator>
  );
}

export default ProductStack;
