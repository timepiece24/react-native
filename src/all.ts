import AsyncStorage from '@react-native-community/async-storage';
import Layout from './components/templates/Layout';
import Loader from './components/elements/Loader';

import { useNavigation } from '@react-navigation/core';
import RNRestart from 'react-native-restart';
import { useDispatch, useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { useTheme } from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Localize from 'src/translations';

/// general icons
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconAntFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconAntFeather from 'react-native-vector-icons/Feather';
import IconAntMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconAntFontiso from 'react-native-vector-icons/Fontisto';
import IconAntIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';

///Icons
import IconLogo from './assets/icons/Logo.svg';
import IconWatch from './assets/icons/Watch.svg';

export {
  LinearGradient,
  useDispatch,
  useSelector,
  useTheme,
  AsyncStorage,
  RNRestart,
  wp,
  hp,
  Layout,
  Loader,
  useNavigation,
  Localize,
  /// general icons
  IconEntypo,
  IconAntDesign,
  IconMaterialIcons,
  IconAntIonicons,
  IconAntFontiso,
  IconAntFontAwesome,
  IconAntFeather,
  IconAntMaterialCommunityIcons,
  ///Icons
  IconLogo,
  IconWatch,
};
