import { AsyncStorage } from '../all';

const saveToStorage = async (key: string, value: string) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log(key, ' failed to save');
    console.log(e, ' failed to save');
  }
};
const removeFromStorage = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    console.log(key, ' failed to remove', e);
  }
};

export { saveToStorage, removeFromStorage };
