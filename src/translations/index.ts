import i18n from 'i18n-js';

import AsyncStorage from '@react-native-community/async-storage';

import en from './en.json';
import ar from './ar.json';

const translationGetter: any = { en, ar };

AsyncStorage.getItem('lang').then((value): void => {
  if (value) {
    i18n.locale = value;
    i18n.fallbacks = true;
    i18n.translations = translationGetter;
  }
});

export default i18n;
