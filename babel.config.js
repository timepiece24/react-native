module.exports = {
  plugins: [
    [
      'module-resolver',
      {
        extensions: [
          '.ts',
          '.tsx',
          '.js',
          '.jsx',
          '.ios.js',
          '.android.js',
          '.json',
        ],
        root: ['.'],
        alias: {
          api: './src/api',
          state: './src/state',
        },
      },
    ],
  ],
  presets: ['module:metro-react-native-babel-preset'],
};
